package com.lebuy;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.gson.Gson;
import com.lebuy.constants.Contants;
import com.lebuy.fragments.MediaFragment;
import com.lebuy.fragments.StoreFragmentListner;
import com.lebuy.fragments.StoreRegistrationFragment1;
import com.lebuy.models.LoginResponse;
import com.lebuy.models.SellerData;
import com.lebuy.models.StoreData;
import com.lebuy.models.StoreResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class StoreRegistrationActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, StoreFragmentListner {
    private MediaFragment mediaFragment;
    private StoreRegistrationFragment1 registrationFragment;
    private StoreData storeData ;
    private GoogleApiClient mGoogleApiClient;
    private boolean storeUpdate;
    TextView tvTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_registration);
        addGoogleApiClient();
        tvTitle= (TextView) findViewById(R.id.tv_tool_text);
        storeData= StoreData.getInstance();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            StoreData data = (StoreData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (data != null) {
                storeData = data;
                storeUpdate = true;
            }
        }
        if(storeUpdate){
            tvTitle.setText(R.string.title_edit_store);
        }else {
            tvTitle.setText(getString(R.string.title_register_store));
        }
        mediaFragment = new MediaFragment();
        getSupportFragmentManager().beginTransaction().add(mediaFragment, "Get image").commit();

        registrationFragment = StoreRegistrationFragment1.getInstance(storeData, mediaFragment, this);
        addFragment(registrationFragment, false);
    }

    private void addGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage(this, 0, this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    private void addFragment(Fragment fragment, Boolean addtobackstatck) {
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (addtobackstatck)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.lay_fragment_container, fragment).commit();
    }

    SellerData sellerData = null;
    private void uploadData(StoreData storeData) {
        JSONObject jsonObject = new JSONObject();
        try {
            String sellerInstance=Preferences.getData(AppConstants.PREF_KEYS.SELLER_INSTANCE,"");
            if(!"".equals(sellerInstance)){
                Gson gson=new Gson();
                sellerData = gson.fromJson(sellerInstance, SellerData.class);
                jsonObject.put("mobileNo", sellerData.getMobile());
            }else {
                jsonObject.put("mobileNo",Preferences.getData(AppConstants.PREF_KEYS.SELLER_MOBILE,""));
            }
            jsonObject.put("storeName", storeData.getStoreName());
            jsonObject.put("emailId",Preferences.getData(AppConstants.PREF_KEYS.SELLER_EMAIL,""));
            jsonObject.put("storeTypeId", storeData.getStoreTypeId());
            jsonObject.put("storeAddress", storeData.getStoreAddress());
            jsonObject.put("latitude", String.valueOf(storeData.getLatitude()));
            jsonObject.put("longitude", String.valueOf(storeData.getLongitude()));
            jsonObject.put("deliveryLimit", String.valueOf(storeData.getDeliveryLimit()));
            jsonObject.put("acceptLimit", String.valueOf(storeData.getAcceptLimit()));
            jsonObject.put("shopOpenTime", storeData.getShopOpenTime());
            jsonObject.put("shopCloseTime", storeData.getShopCloseTime());
            jsonObject.put("minOrderLimit", storeData.getMinimumOrder());
            jsonObject.put("tryAtHome", storeData.getTryAtHome());
            jsonObject.put("allowCOD", storeData.getAllowCOD());
            jsonObject.put("allowCard", storeData.getAllowCard());
            jsonObject.put("allowSudexo", storeData.getAllowSudexo());
            jsonObject.put("isAttachment", storeData.getIsPic());
            if ("1".equals(storeData.getIsPic())) {
                jsonObject.put("attachBase64", Util.encodeToBase64(storeData.getStoreBitmap(), Bitmap.CompressFormat.JPEG, 100));
                jsonObject.put("picExtn", "JPEG");
            }
            HttpParamObject op = new HttpParamObject();
            op.setClassType(StoreResponse.class);
            if (storeUpdate) {
                jsonObject.put("sellerId", sellerData.getSellerId().toString());
                op.setUrl(Contants.STORE_UPDATE_URL);
                op.setJson(jsonObject.toString());
                op.setPostMethod();
                Log.i("msg","request json: "+jsonObject.toString());
                executeTask(AppConstants.TASK_CODES.TASK_CODE_STORE_UPDATE, op);
            } else {
                jsonObject.put("sellerId", Preferences.getData(AppConstants.PREF_KEYS.USER_ID, ""));
                op.setUrl(Contants.STORE_REGISTRATION_URL);
                op.setJson(jsonObject.toString());
                op.setPostMethod();
                Log.i("msg","request json: "+jsonObject.toString());
                executeTask(AppConstants.TASK_CODES.TASK_CODE_STORE_REG, op);
            }
        } catch (JSONException e) {

        }
    }
    private void updateProfile() {
        String sellerInstance = Preferences.getData(AppConstants.PREF_KEYS.SELLER_INSTANCE, "");
        if (!"".equals(sellerInstance)) {
            Gson gson = new Gson();
            final SellerData sellerData = gson.fromJson(sellerInstance, SellerData.class);
            JSONObject object = new JSONObject();
            try {
                object.put("emailId", sellerData.getEmail());
                object.put("password", sellerData.getPassword());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            HttpParamObject op = new HttpParamObject();
            op.setUrl(Contants.LOGIN_URL);
            op.setJson(object.toString());
            op.setPostMethod();
            op.setClassType(LoginResponse.class);
            executeTask(AppConstants.TASK_CODES.TASK_CODE_LOGIN, op);
        }
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            Toast.makeText(StoreRegistrationActivity.this, "Server not responding..!", Toast.LENGTH_SHORT).show();
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.REQ_CODE_GET_PRODUCT:
                StoreResponse storeResponse = (StoreResponse) response;
                if (storeResponse.getRegistrationId() > 0) {
                    Toast.makeText(StoreRegistrationActivity.this, "Successfully register...!", Toast.LENGTH_SHORT).show();
                    getSupportFragmentManager().popBackStack();
                    getSupportFragmentManager().popBackStack();
                    getSupportFragmentManager().popBackStack();
                    Preferences.saveData(AppConstants.PREF_KEYS.STORE_ID,storeResponse.getRegistrationId());
                    finish();
                } else {
                    Toast.makeText(StoreRegistrationActivity.this, "Error: " + response.toString(), Toast.LENGTH_SHORT).show();
                }
                break;
            case AppConstants.TASK_CODES.TASK_CODE_STORE_UPDATE:
                StoreResponse storeResponse1= (StoreResponse) response;
                if("N0000".equals(storeResponse1.getStatusCode())){
                    updateProfile();
                }else {
                    Toast.makeText(StoreRegistrationActivity.this, "Error: " + response.toString(), Toast.LENGTH_SHORT).show();
                }
                break;
            case AppConstants.TASK_CODES.TASK_CODE_LOGIN:
                LoginResponse loginResponse = (LoginResponse) response;
                if (loginResponse.getStatusCode().equals("N0000")) {
                    Gson gson = new Gson();
                    List<SellerData> sellerDataList = loginResponse.getSeller();
                    SellerData sellerData = sellerDataList.get(0);
                    String sellerInstance = gson.toJson(sellerData);
                    Preferences.saveData(AppConstants.PREF_KEYS.SELLER_INSTANCE, sellerInstance);
                    Preferences.saveData(AppConstants.PREF_KEYS.SELLER_NAME, sellerData.getName());
                    final String loginResponceString = gson.toJson(response);
                    Preferences.saveData(AppConstants.PREF_KEYS.LOGIN_INSTANCE, loginResponceString);
                    showToast(getString(R.string.success_update));
                    finish();
                } else {
                    Toast.makeText(this, "Error: " + response.toString(), Toast.LENGTH_SHORT).show();
                    return;
                }
        }
    }

    @Override
    protected void onStart() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void save(StoreData storeData) {
        uploadData(storeData);
    }

    @Override
    public void saveAndNext(Fragment fragment) {
        addFragment(fragment, true);
    }
}
