package com.lebuy;

import android.app.Application;

import java.util.HashMap;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by nitin on 14/03/16.
 */
public class AppController extends Application {

    private static AppController instance;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Preferences.initSharedPreferences(this);
    }

    public static AppController getInstance(){
        return instance;
    }
}
