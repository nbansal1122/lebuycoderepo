package com.lebuy;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.lebuy.constants.Contants;
import com.lebuy.fragments.ProductFragment;
import com.lebuy.models.BaseAPI;
import com.lebuy.models.Category;
import com.lebuy.models.LoginResponse;
import com.lebuy.models.Product;
import com.lebuy.models.SubCategories;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class ProductSubCategoriesActivity extends BaseActivity implements CustomPagerAdapter.PagerAdapterInterface<String>, ProductFragment.ProductFragmentListener {
    TabLayout tabLayout;
    ViewPager viewPager;
    List<String> tabs = new ArrayList();
    List<ProductFragment> productFragments = new ArrayList();
    HashSet<Product> productHashSet;
    //    HashSet<Product> productSelectedHashSet;
    private CustomPagerAdapter<String> adapter;
    Button btnSelect;
    private String storeId;
    private LoginResponse loginResponse;
    private String sellerId;
    private long catId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market);
        initToolBar("Select Products");
        productHashSet = new HashSet();
//        productSelectedHashSet = new HashSet();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
//            ArrayList<Product> products= (ArrayList<Product>) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
//            if(products!=null){
//                for(Product product:products){
//                    productSelectedHashSet.add(product);
//                }
//            }
        }
        Gson gson = new Gson();
        final String data = Preferences.getData(AppConstants.PREF_KEYS.LOGIN_INSTANCE, "");
        loginResponse = gson.fromJson(data, LoginResponse.class);
        sellerId = loginResponse.getSeller().get(0).getSellerId();
        tabLayout = (TabLayout) findViewById(R.id.tab_lay_market);
        viewPager = (ViewPager) findViewById(R.id.view_pager_market);
        btnSelect = (Button) findViewById(R.id.btn_select);
        setOnClickListener(R.id.btn_select);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        catId = bundle.getLong(AppConstants.BUNDLE_KEYS.CATEGORY_ID);

        getAllCategories();

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_select) {
            addProducts(productHashSet);
        }
    }

    private void addProducts(HashSet<Product> productHashSet) {
        JSONObject jsonObject = new JSONObject();
        try {
            if (loginResponse == null) {
                showToast("Some error is occur...!");
                return;
            }
            JSONArray jsonArray = new JSONArray();
            for (Product product : productHashSet) {
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("storeId", storeId);
                jsonObject1.put("subcategoryId", product.getSubcategoryId());
                jsonObject1.put("prdId", product.getProductId());
                jsonObject1.put("sellerId", product.getSellerId());
                jsonArray.put(jsonObject1);
            }
            jsonObject.put("products", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpParamObject op = new HttpParamObject();
        op.setUrl(Contants.ADD_PRODUCTS_URL);
        op.setJson(jsonObject.toString());
        op.setPostMethod();
        executeTask(AppConstants.TASK_CODE_ADD_PRODUCTS, op);
    }

    private void getAllCategories() {
        JSONObject jsonObject = new JSONObject();
        try {
            String userInstance = Preferences.getData(AppConstants.PREF_KEYS.LOGIN_INSTANCE, "");
            if (!"".equals(userInstance)) {
                Gson gson = new Gson();
                LoginResponse loginResponse = gson.fromJson(userInstance, LoginResponse.class);
                storeId = loginResponse.getStoreId();
                jsonObject.put("storeId", storeId);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpParamObject op = new HttpParamObject();
        op.setUrl(Contants.GET_SUB_CATEGORY_URL);
        op.setJson(jsonObject.toString());
        op.setClassType(SubCategories.class);
        op.setPostMethod();
        executeTask(AppConstants.TASK_CODES.TASK_CODE_GET_ALL_SUB_CATEGORIES, op);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        Log.i("msg", "response" + response);
        if (response != null && taskCode == AppConstants.TASK_CODES.TASK_CODE_GET_ALL_SUB_CATEGORIES) {
            SubCategories productCategories = (SubCategories) response;
            if (productCategories != null) {
                SubCategories categories = filterSubCategories(productCategories);
                setData(categories);
            }
        }
        if (response != null && taskCode == AppConstants.TASK_CODE_ADD_PRODUCTS) {
            Gson gson = new Gson();
            BaseAPI baseAPI = gson.fromJson(response.toString(), BaseAPI.class);
            if (baseAPI.getStatusCode().equals("N0000")) {
                showToast("Products successfully selected...!");
                setResult(RESULT_OK);
                finish();
            } else {
                showToast(response.toString());
            }
        }
    }

    private SubCategories filterSubCategories(SubCategories productCategories) {
        SubCategories newSubCategories = new SubCategories();
        List<Category> newCategories = new ArrayList<>();
        Iterator<Category> iterator = productCategories.getSubcategories().iterator();
        while (iterator.hasNext()) {
            Category category = iterator.next();
            if (catId == category.getCategoryId()) {
                newCategories.add(category);
            }
        }
        newSubCategories.setSubcategories(newCategories);
        return newSubCategories;
    }

    public void setData(SubCategories data) {
        final List<Category> categories = data.getSubcategories();
        if(categories.size()<1){
            onNoSubCategories();
            return;
        }else if(categories.size()<3){
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
        }
        if (categories != null) {
            for (int x = 0; x < categories.size(); x++) {
                Category category = categories.get(x);
                tabs.add(category.getDisplayName());
                ProductFragment productFragment = ProductFragment.getInstance(category.getId(), productHashSet, this, x + 10, sellerId);
                productFragments.add(productFragment);
            }
            adapter = new CustomPagerAdapter<>(getSupportFragmentManager(), tabs, this);
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    private void onNoSubCategories() {
        tabLayout.setVisibility(View.GONE);
        initToolBar("No Products are available");
    }

    @Override
    public Fragment getFragmentItem(int position, String listItem) {
        return productFragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position, String listItem) {
        return listItem;
    }

    @Override
    public void onSelectProduct() {
        if (productHashSet.size() > 0) {
            btnSelect.setVisibility(View.VISIBLE);
        } else {
            btnSelect.setVisibility(View.GONE);
        }
    }
}
