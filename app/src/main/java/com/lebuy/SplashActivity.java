package com.lebuy;

import android.app.Activity;
import android.os.Bundle;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.SplashTask;

public class SplashActivity extends BaseActivity implements SplashTask.OnCompliteListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new SplashTask(2000,this).execute();
    }

    @Override
    public void onComplete() {
        final Boolean isLoggedIn = Preferences.getData(AppConstants.PREF_KEYS.IS_LOGIN, false);
        if(isLoggedIn){
            startNextActivity(HomeActivity.class);
        }else {
            startNextActivity(LoginActivity.class);
        }
        finish();
    }
}
