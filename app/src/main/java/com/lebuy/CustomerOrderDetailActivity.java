package com.lebuy;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lebuy.models.OrderDetail;
import com.lebuy.models.OrderItem;
import com.squareup.picasso.Picasso;

import java.util.Iterator;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class CustomerOrderDetailActivity extends BaseActivity {
    TextView tvTitle,tvOrderStatus,tvOrderId,tvDateOfOrder,tvDateOfDelivery,tv_orderTotal;
    TextView tvSubTotal,tvVat,tvTotal;
    LinearLayout layOrderItemsContainer;
    ImageView ivProfile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_order_detail);
        findViews();

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null)
        loadBunble(bundle);
        setOnClickListener(R.id.iv_back);
    }

    private void findViews() {
        tvTitle= (TextView) findViewById(R.id.tv_tool_text);
        tvOrderStatus= (TextView) findViewById(R.id.tv_order_status);
        tvOrderId= (TextView) findViewById(R.id.tv_order_id);
        tvDateOfOrder= (TextView) findViewById(R.id.tv_date_of_order);
        tvDateOfDelivery= (TextView) findViewById(R.id.tv_date_of_delivery);
        tv_orderTotal= (TextView) findViewById(R.id.tv_order_total);

        tvSubTotal= (TextView) findViewById(R.id.tv_sub_total);
        tvVat= (TextView) findViewById(R.id.tv_vate);
        tvTotal= (TextView) findViewById(R.id.tv_total);

        layOrderItemsContainer= (LinearLayout) findViewById(R.id.lay_order_detail_container);
        ivProfile= (ImageView) findViewById(R.id.iv_profile);
    }

    private void loadBunble(Bundle bundle) {
        OrderDetail orderDetail= (OrderDetail) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        Picasso.with(this).load(orderDetail.getImageURL()).into(ivProfile);
        tvTitle.setText(orderDetail.getCustomerName());
        tvOrderStatus.setText(orderDetail.getOrderStatus());
        tvDateOfOrder.setText(orderDetail.getOrderDate());
        tvDateOfDelivery.setText(orderDetail.getDeliveredDate());
        tvOrderId.setText("Order Id: "+orderDetail.getOrderId());
        tv_orderTotal.setText(orderDetail.getTotalAmmount());

        tvSubTotal.setText(orderDetail.getSubTotalAmmount());
        tvVat.setText(orderDetail.getVatAmmount());
        tvTotal.setText(orderDetail.getTotalAmmount());

        List<OrderItem> orderItems=orderDetail.getOrderItems();

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }



}
