package com.lebuy;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.gson.Gson;
import com.lebuy.constants.Contants;
import com.lebuy.models.BaseAPI;
import com.lebuy.models.LoginResponse;
import com.lebuy.models.Product;
import com.lebuy.models.ProductsBySubCategory;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class MyListingActivity extends BaseActivity implements CustomListAdapterInterface {
    ListView listView;
    private CustomListAdapter listAdapter;
    private ArrayList<Product> products = new ArrayList<>();
    private FrameLayout frameBack;
    boolean isFbtnOpen = false;
    private FloatingActionsMenu floatingActionsMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_listing);
        initToolBar("");
        setOnClickListener( R.id.frame_background,R.id.action_add_custom,R.id.action_add_standard);
        listView = (ListView) findViewById(R.id.list_myproducts);
        listView.setEmptyView(findViewById(R.id.tv_empty));
        listAdapter = new CustomListAdapter(this, R.layout.row_my_listing, this.products, this);
        listView.setAdapter(listAdapter);
        frameBack = (FrameLayout) findViewById(R.id.frame_background);
        floatingActionsMenu= (FloatingActionsMenu) findViewById(R.id.f_btn_my_listng);

        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                frameBack.setVisibility(View.VISIBLE);
            }

            @Override
            public void onMenuCollapsed() {
                frameBack.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getData();
    }

    private void getData() {
        JSONObject jsonObject = new JSONObject();
        try {
            final String data = Preferences.getData(AppConstants.PREF_KEYS.LOGIN_INSTANCE, "");
            Gson gson = new Gson();
            LoginResponse loginResponse = gson.fromJson(data, LoginResponse.class);
            if (loginResponse == null) {
                showToast("Some error is occur...!");
                return;
            }
            jsonObject.put("storeId", loginResponse.getStoreId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpParamObject op = new HttpParamObject();
        op.setUrl(Contants.GET_MY_LISTING_URL);
        op.setClassType(ProductsBySubCategory.class);
        op.setJson(jsonObject.toString());
        op.setPostMethod();
        executeTask(AppConstants.TASK_CODE_GET_MY_LISTING, op);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null && taskCode == AppConstants.TASK_CODE_GET_MY_LISTING) {
            ProductsBySubCategory productsBySubCategory = (ProductsBySubCategory) response;
            List<Product> products = productsBySubCategory.getProducts();
            if (products == null) {
                return;
            }
            this.products.clear();
            this.products.addAll(products);
            listAdapter.notifyDataSetChanged();
        }
        if (response != null && taskCode == AppConstants.TASK_CODE_ADD_PRODUCTS_BY_QUANTITY) {
            BaseAPI baseAPI = (BaseAPI) response;
            if (baseAPI.getStatusCode().equals("N0000")) {
                getData();
            } else {
                showToast("Some error is occur..!");
            }
        }
    }

    @Override
    public void onClick(View v) {
        floatingActionsMenu.collapse();
        switch (v.getId()) {
            case R.id.frame_background:
                frameBack.setVisibility(View.GONE);
                break;
            case R.id.action_add_custom:
                getFilterProducts();
                break;
            case R.id.action_add_standard:
                startNextActivity(AddListingActivity.class);
                break;
        }
    }

    private void getFilterProducts() {

        Intent intent = new Intent(this, ProductCategoriesActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, products);
        intent.putExtras(bundle);
        startActivityForResult(intent, AppConstants.REQUEST_CODES.REQ_CODE_GET_PRODUCT);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent, final int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this).inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final Product product = products.get(position);
        holder.tvName.setText(product.getProductName());
        holder.tvMRP.setText("\u20B9 " + product.getMRP());
        holder.tvNetPrize.setText("\u20B9 " + product.getNetPrize());
        holder.tvUnitQuantity.setText("Unit Qty: " + String.valueOf(product.getPriceQuantity()));
        if (product.getPoductQuantity() < 1) {
            holder.tvQuantity.setVisibility(View.GONE);
        } else {
            holder.tvQuantity.setVisibility(View.VISIBLE);
            holder.tvQuantity.setText("Quantity: " + product.getPoductQuantity());
        }
        String imgUrl = product.getPicUrl().trim();
        if (!TextUtils.isEmpty(imgUrl))
            Picasso.with(this).load(imgUrl).into(holder.ivProduct);
        holder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(product);
            }
        });

        return convertView;
    }

    public void showDialog(final Product product) {
        final TextView tvName, tvMRP, tvNetPrize, tvUnitQuantity, tvQuantity;
        ImageView ivProduct;
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_product);
        tvName = (TextView) dialog.findViewById(R.id.tv_product_name);
        tvMRP = (TextView) dialog.findViewById(R.id.tv_product_MRP);
        tvUnitQuantity = (TextView) dialog.findViewById(R.id.tv_product_price_quantity);
        tvNetPrize = (TextView) dialog.findViewById(R.id.tv_product_netprize);
        tvQuantity = (TextView) dialog.findViewById(R.id.tv_product_quantity);
        ivProduct = (ImageView) dialog.findViewById(R.id.iv_product_row);
        tvName.setText(product.getProductName());
        tvMRP.setText("\u20B9 " + product.getMRP());
        tvNetPrize.setText("\u20B9 " + product.getNetPrize());
        tvUnitQuantity.setText("Unit Qty: " + String.valueOf(product.getPriceQuantity()));
        if (product.getPoductQuantity() < 0) {
            tvQuantity.setText("0");
        } else {
            tvQuantity.setText(String.valueOf(product.getPoductQuantity()));
        }
        String imgUrl = product.getPicUrl().trim();
        if (!TextUtils.isEmpty(imgUrl))
            Picasso.with(this).load(imgUrl).into(ivProduct);
        dialog.findViewById(R.id.btn_dialog_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                addProductByQuantity(product);
            }
        });
        dialog.findViewById(R.id.btn_dialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.iv_minus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (product.getPoductQuantity() > 1) {
                    product.setPoductQuantity(product.getPoductQuantity() - 1);
                    tvQuantity.setText(String.valueOf(product.getPoductQuantity()));
                }
            }
        });
        dialog.findViewById(R.id.iv_plus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                product.setPoductQuantity(product.getPoductQuantity() + 1);
                tvQuantity.setText(String.valueOf(product.getPoductQuantity()));
            }
        });

        dialog.show();
    }


    class Holder {
        TextView tvName, tvMRP, tvNetPrize, tvUnitQuantity, tvQuantity;
        ImageView ivProduct;
        Button btnAdd;

        public Holder(View view) {
            tvName = (TextView) view.findViewById(R.id.tv_product_name);
            tvMRP = (TextView) view.findViewById(R.id.tv_product_MRP);
            tvUnitQuantity = (TextView) view.findViewById(R.id.tv_quantity);
            tvNetPrize = (TextView) view.findViewById(R.id.tv_product_netprize);
            tvQuantity = (TextView) view.findViewById(R.id.tv_quantity);
            ivProduct = (ImageView) view.findViewById(R.id.iv_product_row);
            btnAdd = (Button) view.findViewById(R.id.btn_add_product);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == AppConstants.REQUEST_CODES.REQ_CODE_GET_PRODUCT) {
            getData();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_my_listing, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.action_filter) {
//            getFilterProducts();
//        }
//        return false;
//    }

    private void addProductByQuantity(Product product) {
        JSONObject jsonObject = new JSONObject();
        try {
            Gson gson = new Gson();
            final String data = Preferences.getData(AppConstants.PREF_KEYS.LOGIN_INSTANCE, "");
            LoginResponse loginResponse = gson.fromJson(data, LoginResponse.class);
            if (loginResponse == null) {
                showToast("Some error is occur...!");
                return;
            }
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("storeId", loginResponse.getStoreId());
            jsonObject1.put("prdId", product.getProductId());
            jsonObject1.put("quantity", String.valueOf(product.getPoductQuantity()));
            jsonArray.put(jsonObject1);
            jsonObject.put("products", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpParamObject op = new HttpParamObject();
        op.setUrl(Contants.ADD_PRODUCTS_BY_QUANTY);
        op.setJson(jsonObject.toString());
        op.setClassType(BaseAPI.class);
        op.setPostMethod();
        executeTask(AppConstants.TASK_CODE_ADD_PRODUCTS_BY_QUANTITY, op);
    }

}
