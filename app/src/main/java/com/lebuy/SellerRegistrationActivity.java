package com.lebuy;


import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lebuy.constants.Contants;
import com.lebuy.fragments.MediaFragment;
import com.lebuy.fragments.SellerRegisterFragment;
import com.lebuy.models.LoginResponse;
import com.lebuy.models.SellerData;
import com.lebuy.models.SellerRegisterResponse;
import com.lebuy.models.StoreData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class SellerRegistrationActivity extends BaseActivity implements SellerRegisterFragment.RegisterFragmentListner {
    SellerData sellerData = new SellerData();
    boolean updateProfile;
    private MediaFragment mediaFragment;
    private StoreData data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_registration);
//-----------------In case of Update profile--------------------//
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            data = (StoreData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (data != null) {
                sellerData = data.getSeller().get(0);
                updateProfile = true;
            }
        }
//-------------------------------------------------------------//
        mediaFragment = new MediaFragment();//get Image from camera and gallery...
        getSupportFragmentManager().beginTransaction().add(mediaFragment, "Get image").commit();
        SellerRegisterFragment registrationFragment1 = SellerRegisterFragment.getInstance(sellerData, mediaFragment, this, updateProfile);
        addFragment(registrationFragment1, false);
    }

    private void addFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (addToBackStack)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.container, fragment).commit();
    }

    @Override
    public void onSellerDataRecieve() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("sellerName", sellerData.getName());
            obj.put("mobileNo", sellerData.getMobile());
            obj.put("emailId", sellerData.getEmail());
            obj.put("password", sellerData.getPassword());
            Bitmap bitmap = sellerData.getImageBitmap();
            if (bitmap != null) {
                obj.put("isAttachment", "1");
                obj.put("picExtn", "PNG");
                String imageString = Util.encodeToBase64(bitmap, Bitmap.CompressFormat.PNG, 100);
                obj.put("attachBase64", imageString);
            } else {
                obj.put("isAttachment", "0");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpParamObject op = new HttpParamObject();
        if (updateProfile) {
            op.setUrl(Contants.UPDATE_SELLER_URL);
        } else {
            op.setUrl(Contants.SIGN_UP_URL);
        }
        op.setJson(obj.toString());
        op.setPostMethod();
        op.setClassType(SellerRegisterResponse.class);
        Log.i("msg", "json: " + obj.toString());
        executeTask(AppConstants.TASK_CODE_REGISTER, op);
    }

    private void updateProfile() {
        String sellerInstance = Preferences.getData(AppConstants.PREF_KEYS.SELLER_INSTANCE, "");
        if (!"".equals(sellerInstance)) {
            Gson gson = new Gson();
            final SellerData sellerData = gson.fromJson(sellerInstance, SellerData.class);
            JSONObject object = new JSONObject();
            try {
                object.put("emailId", sellerData.getEmail());
                object.put("password", sellerData.getPassword());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            HttpParamObject op = new HttpParamObject();
            op.setUrl(Contants.LOGIN_URL);
            op.setJson(object.toString());
            op.setPostMethod();
            op.setClassType(LoginResponse.class);
            executeTask(AppConstants.TASK_CODES.TASK_CODE_LOGIN, op);
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.TASK_CODE_LOGIN:
                LoginResponse loginResponse = (LoginResponse) response;
                if (loginResponse.getStatusCode().equals("N0000")) {
                    Gson gson = new Gson();
                    List<SellerData> sellerDataList = loginResponse.getSeller();
                    SellerData sellerData = sellerDataList.get(0);
                    String sellerInstance = gson.toJson(sellerData);
                    Preferences.saveData(AppConstants.PREF_KEYS.SELLER_INSTANCE, sellerInstance);
                    Preferences.saveData(AppConstants.PREF_KEYS.SELLER_NAME, sellerData.getName());
                    final String loginResponceString = gson.toJson(response);
                    Preferences.saveData(AppConstants.PREF_KEYS.LOGIN_INSTANCE, loginResponceString);
                } else {
                    Toast.makeText(this, "Error: " + response.toString(), Toast.LENGTH_SHORT).show();
                    return;
                }
                showToast(getString(R.string.success_update));
                finish();
                break;
            case AppConstants.TASK_CODE_REGISTER:
                SellerRegisterResponse sellerRegisterResponse = (SellerRegisterResponse) response;
                if (sellerRegisterResponse.getStatusCode().equals("N0000")) {
                    Preferences.saveData(AppConstants.PREF_KEYS.USER_ID, sellerRegisterResponse.getRegistrationId().toString());
                    if (updateProfile) {
                        updateProfile();
                    } else {
                        Intent intent = new Intent(this, DigitAuthActivity.class);
                        startActivityForResult(intent, AppConstants.REQ_CODE_OTP);
                    }
                } else {
                    String error = sellerRegisterResponse.getStatusCode();
                    String errorMsg = error.substring(error.indexOf(':') + 2, error.length() - 1);
                    showToast(errorMsg);
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.REQ_CODE_OTP && resultCode == RESULT_OK) {
            Toast.makeText(SellerRegistrationActivity.this, "Successfully registered...!", Toast.LENGTH_SHORT).show();
            if (updateProfile) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, this.data);
                startNextActivity(bundle, StoreRegistrationActivity.class);
            } else {
                startNextActivity(StoreRegistrationActivity.class);
            }
            finish();
        }
    }
}
