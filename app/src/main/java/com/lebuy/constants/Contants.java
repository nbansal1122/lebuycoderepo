package com.lebuy.constants;

/**
 * Created by Admin on 17-Apr-16.
 */
public interface Contants {
    String SIGN_UP_URL = "http://52.32.192.185:8080/nearby/seller/signUp";
    String VERIFY_DIGIT_URL = "http://52.32.192.185:8080/nearby/seller/verifyPhone";
    String LOGIN_URL = "http://52.32.192.185:8080/nearby/seller/appLogin";
    String STORE_REGISTRATION_URL = "http://52.32.192.185:8080/nearby/seller/registerStore";
    String STORE_UPDATE_URL = "http://52.32.192.185:8080/nearby/seller/updateStore";
    String GET_STORE_BY_SELLER_ID = "http://52.32.192.185:8080/nearby/seller/getStoreBySellerId/";
    String GET_ALL_CATEGORY_URL = "http://52.32.192.185:8080/nearby/seller/getAllCategories";

    String GET_SUB_CATEGORY_URL = "http://52.32.192.185:8080/nearby/seller/getAllSubCategories";
    String GET_PRODUCTS_BY_SUBCATEGORY_ID_URL = "http://52.32.192.185:8080/nearby/seller/getProductTemplatesBySubCategoryId";
    String ADD_PRODUCTS_URL = "http://52.32.192.185:8080/nearby/seller/addProductsByStore";
    String GET_MY_LISTING_URL = "http://52.32.192.185:8080/nearby/seller/getAllProductsByStore";
    String GET_STORE_TYPE_URL = "http://52.32.192.185:8080/nearby/seller/getStoreTypes";
    String UPDATE_SELLER_URL = "http://52.32.192.185:8080/nearby/seller/updateSeller";
    String GET_AUTH_CODE_URL = "http://52.32.192.185:8080/nearby/seller/sendAuthCode";
    String ADD_PRODUCTS_BY_QUANTY = "http://52.32.192.185:8080/nearby/seller/addProductsQuantity";
}
