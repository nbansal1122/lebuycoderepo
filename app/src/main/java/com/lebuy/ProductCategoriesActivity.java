package com.lebuy;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.lebuy.constants.Contants;
import com.lebuy.models.AllCategories;
import com.lebuy.models.Category;
import com.lebuy.models.Product;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class ProductCategoriesActivity extends BaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    List<Category> categories = new ArrayList();
    private CustomListAdapter<Category> listAdapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_categories_acitivity);
        initToolBar("Select Category");
        listView = (ListView) findViewById(R.id.list_categories);
        listAdapter = new CustomListAdapter<>(this, R.layout.row_categories, categories, this);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAllCategories();
    }

    private void getAllCategories() {
        HttpParamObject op = new HttpParamObject();
        op.setUrl(Contants.GET_ALL_CATEGORY_URL);
        op.setClassType(AllCategories.class);
        executeTask(AppConstants.TASK_CODES.TASK_CODE_GET_ALL_CATEGORIES, op);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (taskCode == AppConstants.TASK_CODES.TASK_CODE_GET_ALL_CATEGORIES && response != null) {
            AllCategories allCategories = (AllCategories) response;
            categories.clear();
            categories.addAll(allCategories.getCategories());
            listAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this).inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final Category category = categories.get(position);
        holder.textView.setText(category.getDisplayName());
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle bundle=addSelectedList(position);
        Intent intent = new Intent(this, ProductSubCategoriesActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, AppConstants.REQUEST_CODES.REQ_CODE_GET_PRODUCT);
    }

    private Bundle addSelectedList(int i) {
        Bundle bundle=new Bundle();
        bundle.putLong(AppConstants.BUNDLE_KEYS.CATEGORY_ID, categories.get(i).getId());
//        Bundle extras = getIntent().getExtras();
//        if(extras!=null){
//            ArrayList<Product> products= (ArrayList<Product>) extras.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
//            if(products!=null){
//                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,products);
//            }
//        }
        return bundle;
    }

    class Holder {
        TextView textView;

        public Holder(View view) {
            textView = (TextView) view.findViewById(R.id.tv_row_category);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode==AppConstants.REQUEST_CODES.REQ_CODE_GET_PRODUCT) {
            setResult(RESULT_OK);
            finish();
        }
    }
}
