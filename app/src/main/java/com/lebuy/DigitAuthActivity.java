package com.lebuy;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.lebuy.constants.Contants;
import com.lebuy.models.DigitAuthRespoce;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class DigitAuthActivity extends BaseActivity {
    private static final int TASK_CODE_VERIFY_NUMBER = 12;
    private static final int TASK_CODE_AUTH_CODE = 13;
    EditText et_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digit_auth);
        et_code = (EditText) findViewById(R.id.et_confirm_code);
        setOnClickListener(R.id.btn_confirm_code, R.id.btn_resend_code);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAuthCode();
    }

    private void getAuthCode() {
        JSONObject object=new JSONObject();
        try {
            object.put("userID", Preferences.getData(AppConstants.PREF_KEYS.USER_ID,"0"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpParamObject op = new HttpParamObject();
        op.setUrl(Contants.GET_AUTH_CODE_URL);
        op.setJson(object.toString());
        op.setPostMethod();
        op.setClassType(DigitAuthRespoce.class);
        executeTask(TASK_CODE_AUTH_CODE, op);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm_code:
                if(et_code.getText().toString().equals("")){
                    et_code.setError("Plz enter code");
                    return;
                }
                if(!Util.isConnectingToInternet(this)){
                    et_code.setError("Plz check your network and try again.");
                    return;
                }
                uploadCode();
                break;
            case R.id.btn_resend_code:
                if(!Util.isConnectingToInternet(this)){
                    showToast("Plz check your network and try again.");
                    return;
                }
                break;
        }
    }
    private void uploadCode(){
        JSONObject object=new JSONObject();
        try {
            object.put("userID", Preferences.getData(AppConstants.PREF_KEYS.USER_ID,"0"));
            object.put("authcode",et_code.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpParamObject op = new HttpParamObject();
        op.setUrl(Contants.VERIFY_DIGIT_URL);
        op.setJson(object.toString());
        op.setPostMethod();
        op.setClassType(DigitAuthRespoce.class);
        executeTask(TASK_CODE_VERIFY_NUMBER, op);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response,taskCode,params);
        if(taskCode== TASK_CODE_VERIFY_NUMBER &&response!=null){
            DigitAuthRespoce authResponce= (DigitAuthRespoce) response;
            Log.i("msg","responce ="+response.toString());
            if(authResponce.getStatusCode().equals("N0000")){
                setResult(RESULT_OK);
                finish();
            }else {
                Toast.makeText(DigitAuthActivity.this, "Failed to verify plz try again", Toast.LENGTH_SHORT).show();
            }
        }
        if(taskCode== TASK_CODE_AUTH_CODE &&response!=null){

        }
    }
}
