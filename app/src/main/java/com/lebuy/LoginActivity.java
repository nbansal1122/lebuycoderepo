package com.lebuy;

import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lebuy.constants.Contants;
import com.lebuy.models.LoginResponse;
import com.lebuy.models.SellerData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class LoginActivity extends BaseActivity {
    private static final int TASK_CODE_LOGIN = 20;
    TextInputLayout tilEmail, tilPassword;
    Boolean cheakRemember = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        tilEmail = (TextInputLayout) findViewById(R.id.til_email);
        tilPassword = (TextInputLayout) findViewById(R.id.til_password);
        setOnClickListener(R.id.tv_forgot_pass, R.id.tv_register, R.id.btn_sign_in);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_forgot_pass:
                break;
            case R.id.tv_register:
                startNextActivity(SellerRegistrationActivity.class);
                break;
            case R.id.btn_sign_in:
                login();
                break;
        }
    }

    private void login() {
        String email = tilEmail.getEditText().getText().toString();
        String password = tilPassword.getEditText().getText().toString();
        if (TextUtils.isEmpty(email)) {
            tilEmail.setError("Please enter e-mail");
            return;
        }
        if (!Util.isValidEmail(email)) {
            tilEmail.setError("Invalid e-mail");
            return;
        }
        if (TextUtils.isEmpty(password)) {
            tilPassword.setError("Please enter password");
            return;
        }
        JSONObject object = new JSONObject();
        try {
            object.put("emailId", email);
            object.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpParamObject op = new HttpParamObject();
        op.setUrl(Contants.LOGIN_URL);
        op.setJson(object.toString());
        op.setPostMethod();
        op.setClassType(LoginResponse.class);
        executeTask(TASK_CODE_LOGIN, op);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (taskCode == TASK_CODE_LOGIN) {
            Gson gson = new Gson();
            if (response == null) {
                showToast(R.string.error_mag_server_not_respond);
                return;
            }
            LoginResponse loginResponse = (LoginResponse) response;
            if (loginResponse.getStatusCode().equals("M10032")) {
                Toast.makeText(LoginActivity.this, "User email or password not match..!", Toast.LENGTH_SHORT).show();
                return;
            }
            if (loginResponse.getStatusCode().equals("N0000")) {
                if (cheakRemember) {
                    Preferences.saveData(AppConstants.PREF_KEYS.IS_LOGIN, true);
                } else {
                    Preferences.saveData(AppConstants.PREF_KEYS.IS_LOGIN, false);
                }
                List<SellerData> sellerDataList = loginResponse.getSeller();
                SellerData sellerData = sellerDataList.get(0);
                String sellerInstance = gson.toJson(sellerData);
                Preferences.saveData(AppConstants.PREF_KEYS.SELLER_INSTANCE, sellerInstance);
                Preferences.saveData(AppConstants.PREF_KEYS.SELLER_EMAIL, tilEmail.getEditText().getText().toString());
                Preferences.saveData(AppConstants.PREF_KEYS.SELLER_NAME, sellerData.getName());
                final String loginResponceString = gson.toJson(response);
                Preferences.saveData(AppConstants.PREF_KEYS.LOGIN_INSTANCE, loginResponceString);
                startNextActivity(HomeActivity.class);
                finish();
                Log.i("msg", "json=" + loginResponceString);
            } else {
                String error = loginResponse.getStatusCode();
                String errorMsg = error.substring(error.indexOf(':') + 2, error.length() - 1);
                Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            }
        }
    }
}