package com.lebuy.models;

import java.util.List;

/**
 * Created by Admin on 25-May-16.
 */
public class ProductsBySubCategory extends BaseAPI {
    List<Product> products;
    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
