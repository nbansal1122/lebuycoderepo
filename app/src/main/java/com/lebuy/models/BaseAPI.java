package com.lebuy.models;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * Created by Admin on 26-Apr-16.
 */
public class BaseAPI implements Serializable {
    public String statusCode;

    private String message = "";
    private boolean isError;

    public String getMessage() {
        isError();
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        if(!TextUtils.isEmpty(statusCode)){
           String[] split = statusCode.split("::");
            if(split.length==2){
                String code = split[0];
                if("N0000".equalsIgnoreCase(code)){
                    return false;
                }
                message = split[1];

            }
        }
        return true;
    }

    public void setError(boolean error) {
        isError = error;
    }



    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
