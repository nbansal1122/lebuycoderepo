package com.lebuy.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 25-May-16.
 */
public class ProductCategory {
    @SerializedName("id")
    long categoryId;
    @SerializedName("subCategoryName")
    String categoryName;
    String displayName;
    String status;

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
