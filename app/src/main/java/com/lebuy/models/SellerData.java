package com.lebuy.models;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Admin on 05-May-16.
 */
public class SellerData implements Serializable {
    @SerializedName("sellerId")
    String sellerId;
    @SerializedName("sellerName")
    String name = "";
    @SerializedName("mobileNo")
    String mobile = "";
    @SerializedName("emailId")
    String email = "";
    @SerializedName("password")
    String password = "";
    @SerializedName("isPic")
    String isPick;
    String imageUrl = "";
    Bitmap imageBitmap;

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIsPick() {
        return isPick;
    }

    public void setIsPick(String isPick) {
        this.isPick = isPick;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }
}
