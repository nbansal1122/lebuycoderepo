package com.lebuy.models;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Admin on 22-Apr-16.
 */
public class StoreData extends LoginResponse implements Serializable{
    Bitmap storeBitmap=null;
    private String loyality="";

    public static StoreData getInstance() {
        return new StoreData();
    }
    public Bitmap getStoreBitmap() {
        return storeBitmap;
    }

    public void setStoreBitmap(Bitmap storeBitmap) {
        this.storeBitmap = storeBitmap;
    }

    public String getLoyality() {
        return loyality;
    }

    public void setLoyality(String loyality) {
        this.loyality = loyality;
    }

}
