package com.lebuy.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Admin on 06-May-16.
 */
public class LoginResponse extends BaseAPI implements Serializable{
    @SerializedName("storeId")
    String storeId="";
    @SerializedName("storeName")
    String storeName="";
    @SerializedName("seller")
    List<SellerData> seller;
    String storeTypeId="";
    @SerializedName("storeAddress")
    String storeAddress="";
    @SerializedName("latitude")
    double latitude=0.0;
    @SerializedName("longitude")
    double longitude=0.0;
    @SerializedName("deliveryLimit")
    long deliveryLimit=0;
    @SerializedName("acceptLimit")
    int acceptLimit=0;
    @SerializedName("shopOpenTime")
    String shopOpenTime="";
    @SerializedName("shopCloseTime")
    String shopCloseTime="";
    @SerializedName("minOrderLimit")
    String minimumOrder="";
    @SerializedName("tryAtHome")
    String tryAtHome="";
    @SerializedName("allowCOD")
    String allowCOD="";
    @SerializedName("allowCard")
    String allowCard="";
    @SerializedName("allowSudexo")
    String allowSudexo="";
    @SerializedName("isPic")
    String isPic="";
    @SerializedName("picUrl")
    String picUrl="";

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public List<SellerData> getSeller() {
        return seller;
    }

    public void setSeller(List<SellerData> seller) {
        this.seller = seller;
    }

    public String getStoreTypeId() {
        return storeTypeId;
    }

    public void setStoreTypeId(String storeTypeId) {
        this.storeTypeId = storeTypeId;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getDeliveryLimit() {
        return deliveryLimit;
    }

    public void setDeliveryLimit(int deliveryLimit) {
        this.deliveryLimit = deliveryLimit;
    }

    public int getAcceptLimit() {
        return acceptLimit;
    }

    public void setAcceptLimit(int acceptLimit) {
        this.acceptLimit = acceptLimit;
    }

    public String getShopOpenTime() {
        return shopOpenTime;
    }

    public void setShopOpenTime(String shopOpenTime) {
        this.shopOpenTime = shopOpenTime;
    }

    public String getShopCloseTime() {
        return shopCloseTime;
    }

    public void setShopCloseTime(String shopCloseTime) {
        this.shopCloseTime = shopCloseTime;
    }

    public String getMinimumOrder() {
        return minimumOrder;
    }

    public void setMinimumOrder(String minimumOrder) {
        this.minimumOrder = minimumOrder;
    }

    public String getTryAtHome() {
        return tryAtHome;
    }

    public void setTryAtHome(String tryAtHome) {
        this.tryAtHome = tryAtHome;
    }

    public String getAllowCOD() {
        return allowCOD;
    }

    public void setAllowCOD(String allowCOD) {
        this.allowCOD = allowCOD;
    }

    public String getAllowCard() {
        return allowCard;
    }

    public void setAllowCard(String allowCard) {
        this.allowCard = allowCard;
    }

    public String getAllowSudexo() {
        return allowSudexo;
    }

    public void setAllowSudexo(String allowSudexo) {
        this.allowSudexo = allowSudexo;
    }

    public String getIsPic() {
        return isPic;
    }

    public void setIsPic(String isPic) {
        this.isPic = isPic;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }
}

