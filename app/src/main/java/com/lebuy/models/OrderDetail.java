package com.lebuy.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Admin on 26-Apr-16.
 */
public class OrderDetail implements Serializable{
    String customerName;
    String mobileNo;
    String imageURL;

    String orderStatus;
    String orderDate;
    String deliveredDate;

    String orderId;
    String totalAmmount;
    String subTotalAmmount;
    String vatAmmount;

    List<OrderItem> orderItems;

    String pickupAddress;
    String deliveryAddress;
    String pickupTime;
    String DeliveryTime;

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(String pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getDeliveryTime() {
        return DeliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        DeliveryTime = deliveryTime;
    }

    public String getSubTotalAmmount() {
        return subTotalAmmount;
    }

    public void setSubTotalAmmount(String subTotalAmmount) {
        this.subTotalAmmount = subTotalAmmount;
    }

    public String getVatAmmount() {
        return vatAmmount;
    }

    public void setVatAmmount(String vatAmmount) {
        this.vatAmmount = vatAmmount;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public static OrderDetail getInstance() {
        return new OrderDetail();
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }


    public String getDeliveredDate() {
        return deliveredDate;
    }

    public void setDeliveredDate(String deliveredDate) {
        this.deliveredDate = deliveredDate;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTotalAmmount() {
        return totalAmmount;
    }

    public void setTotalAmmount(String totalAmmount) {
        this.totalAmmount = totalAmmount;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
