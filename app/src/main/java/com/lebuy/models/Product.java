package com.lebuy.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Admin on 19-May-16.
 */
public class Product implements Serializable{
    @SerializedName("prdId")
    long productId;
    @SerializedName("prdName")
    String productName;
    @SerializedName("quantity")
    int poductQuantity;
    @SerializedName("sellingPrice")
    String netPrize;
    @SerializedName("discountPrice")
    String MRP;
    String priceQuantity;
    long subcategoryId;
    long categoryId;
    String sellerId;
    String storeId;
    String isPic;
    String picUrl;

    public int getPoductQuantity() {
        return poductQuantity;
    }

    public void setPoductQuantity(int poductQuantity) {
        this.poductQuantity = poductQuantity;
    }

    public void setPriceQuantity(String priceQuantity) {
        this.priceQuantity = priceQuantity;
    }

    public String getPriceQuantity() {
        return priceQuantity;
    }

    public void setSubcategoryId(long subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public void setIsPic(String isPic) {
        this.isPic = isPic;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getNetPrize() {
        return netPrize;
    }

    public void setNetPrize(String netPrize) {
        this.netPrize = netPrize;
    }

    public String getMRP() {
        return MRP;
    }

    public void setMRP(String MRP) {

        this.MRP = MRP;
    }
    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getSubcategoryId() {
        return subcategoryId;
    }

    public String getIsPic() {
        return isPic;
    }

    public String getPicUrl() {
        return picUrl;
    }
}
