package com.lebuy.models;

import java.io.Serializable;

/**
 * Created by Admin on 30-Apr-16.
 */
public class OrderItem implements Serializable{
    String itemName;
    String itemRate;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemRate() {
        return itemRate;
    }

    public void setItemRate(String itemRate) {
        this.itemRate = itemRate;
    }
}
