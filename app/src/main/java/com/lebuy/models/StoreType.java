package com.lebuy.models;

/**
 * Created by Admin on 24-Jul-16.
 */
public class StoreType {
    long id;
    String storeType;
    String isEnabled;

    public long getId() {
        return id;
    }

    public String getStoreType() {
        return storeType;
    }

    public String getIsEnabled() {
        return isEnabled;
    }
}
