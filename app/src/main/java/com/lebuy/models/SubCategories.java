package com.lebuy.models;

import java.util.List;

/**
 * Created by Admin on 25-May-16.
 */
public class SubCategories extends BaseAPI {
    List<Category> subcategories;

    public List<Category> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<Category> subcategories) {
        this.subcategories = subcategories;
    }
}
