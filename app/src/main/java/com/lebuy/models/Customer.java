package com.lebuy.models;

/**
 * Created by Admin on 25-Apr-16.
 */
public class Customer {
    String Name;
    String Mobile;
    float rating;
    String detail;
    String imageUrl;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }


    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isFavrite() {
        return isFavrite;
    }

    public void setIsFavrite(boolean isFavrite) {
        this.isFavrite = isFavrite;
    }

    String ImageUrl;
    int likes;
    boolean isFavrite;
}
