package com.lebuy.models;

/**
 * Created by Admin on 23-Apr-16.
 */
public class StoreResponse {
    public String statusCode ;
    public int registrationId ;

    public String getStatusCode() {
        return statusCode;
    }

    public int getRegistrationId() {
        return registrationId;
    }
}
