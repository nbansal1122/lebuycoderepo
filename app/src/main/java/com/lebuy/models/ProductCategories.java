package com.lebuy.models;

import java.util.List;

/**
 * Created by Admin on 19-May-16.
 */
public class ProductCategories extends BaseAPI {
    List<ProductCategory> categories;

    public List<ProductCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<ProductCategory> categories) {
        this.categories = categories;
    }
}
