package com.lebuy.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 19-Apr-16.
 */
public class SellerRegisterResponse extends BaseAPI {

    @SerializedName("registrationId")
    String registrationId;
    public String getRegistrationId() {
        return registrationId;
    }

}
