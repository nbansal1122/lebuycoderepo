package com.lebuy;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

public class FloatingButton {

    public static void addFloatingbutton(final Activity activity, final FrameLayout backfroundFrame){
        DisplayMetrics dm = activity.getResources().getDisplayMetrics();
        int floating_btn_size= (activity.getResources().getInteger(R.integer.floating_ball_size) * (dm.densityDpi / 160));
        int floating_btn_radius= (activity.getResources().getInteger(R.integer.floating_ball_radius) * (dm.densityDpi / 160));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(floating_btn_size, floating_btn_size);

        ImageView icon = new ImageView(activity);
        icon.setImageResource(R.drawable.ic_circle);
        icon.setLayoutParams(params);

        FloatingActionButton actionButton = new FloatingActionButton.Builder(activity)
                .setLayoutParams(new FloatingActionButton.LayoutParams(floating_btn_size,floating_btn_size))
                .setContentView(icon)
                .setBackgroundDrawable(R.drawable.floating_btn_background)
                .build();
        SubActionButton.Builder itemBuilder = new SubActionButton.Builder(activity);
        ImageView itemIcon = new ImageView(activity);
        itemIcon.setImageResource(R.drawable.ic_1);

        ImageView itemIcon1 = new ImageView(activity);
        itemIcon1.setImageResource(R.drawable.ic_2);

        ImageView itemIcon3 = new ImageView(activity);
        itemIcon3.setImageResource(R.drawable.ic_4);

        ImageView itemIcon4 = new ImageView(activity);
        itemIcon4.setImageResource(R.drawable.ic_5);
        SubActionButton b1 = itemBuilder.setContentView(itemIcon).build();
        b1.setLayoutParams(params);
        SubActionButton b2 = itemBuilder.setContentView(itemIcon1).build();
        b2.setLayoutParams(params);
        SubActionButton b4 = itemBuilder.setContentView(itemIcon3).build();
        b4.setLayoutParams(params);
        SubActionButton b5 = itemBuilder.setContentView(itemIcon4).build();
        b5.setLayoutParams(params);

        final FloatingActionMenu actionMenu = new FloatingActionMenu.Builder(activity)
                .addSubActionView(b1)
                .addSubActionView(b2)
                .addSubActionView(b4)
                .addSubActionView(b5)
                .setRadius(floating_btn_radius)
                .attachTo(actionButton)
                .build();

        actionMenu.setStateChangeListener(new FloatingActionMenu.MenuStateChangeListener() {
            @Override
            public void onMenuOpened(FloatingActionMenu floatingActionMenu) {
                backfroundFrame.setVisibility(View.VISIBLE);
            }

            @Override
            public void onMenuClosed(FloatingActionMenu floatingActionMenu) {
                backfroundFrame.setVisibility(View.GONE);
            }
        });
        backfroundFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(backfroundFrame.getVisibility()==View.VISIBLE){
                actionMenu.close(true);
                backfroundFrame.setVisibility(View.GONE);
            }
            }
        });
    }
}
