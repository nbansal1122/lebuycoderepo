package com.lebuy;

import android.os.Bundle;
import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

import simplifii.framework.activity.BaseActivity;

public class AddListingActivity extends BaseActivity {
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_listing);
        spinner = (Spinner) findViewById(R.id.spinner_sub_category);
        String[] list = getResources().getStringArray(R.array.sub_categories);
        ArrayAdapter<String> adapter = new ArrayAdapter(this, R.layout.row_spinner_addlisting, list);
        spinner.setAdapter(adapter);
        initToolBar("");
    }

}
