package com.lebuy;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.lebuy.fragments.DrawerFragment;
import com.lebuy.fragments.OrderFragment;

import java.util.ArrayList;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class HomeActivity extends BaseActivity implements DrawerLayout.DrawerListener {
    private FrameLayout mainContainer;
    private DrawerLayout drawerLayout;
    boolean isDrawerOpen;
    private FragmentManager supportFragmentManager;
    public TextView tvToolText;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        tvToolText = (TextView) findViewById(R.id.tv_tool_text);
        initToolBar("");
        toolbar = (Toolbar) findViewById(R.id.toolbar);


        mainContainer = (FrameLayout) findViewById(R.id.lay_home);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        drawerLayout.addDrawerListener(this);


        FloatingButton.addFloatingbutton(this, mainContainer);
        supportFragmentManager = getSupportFragmentManager();
        addDrawerFragment();
        OrderFragment orderFragment = OrderFragment.getInstance(tvToolText);
        addFragment(orderFragment, false);
    }

    private void addDrawerFragment() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        String[] val = getResources().getStringArray(R.array.drawer_items);
        ArrayList<String> arrayList = new ArrayList<>();
        for (String s : val) {
            arrayList.add(s);
        }
        DrawerFragment drawerFragment = DrawerFragment.getInstance(arrayList, this, drawerLayout);
        supportFragmentManager.beginTransaction().replace(R.id.lay_drawer, drawerFragment).commit();
    }


    public void addFragment(Fragment fragment, boolean addToBachstack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        getSupportFragmentManager().popBackStack();
        if (addToBachstack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.replace(R.id.lay_home_fragment_container, fragment).commit();
    }

    @Override
    public void onBackPressed() {
        if (isDrawerOpen) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            isDrawerOpen = false;
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
            return;
        }
        super.onBackPressed();
    }

    public void logout() {
        Preferences.removeData(AppConstants.PREF_KEYS.USER_ID);
        Preferences.removeData(AppConstants.PREF_KEYS.SELLER_EMAIL);
        Preferences.removeData(AppConstants.PREF_KEYS.SELLER_MOBILE);
        Preferences.removeData(AppConstants.PREF_KEYS.IS_LOGIN);
        Preferences.removeData(AppConstants.PREF_KEYS.SELLER_NAME);
        Preferences.removeData(AppConstants.PREF_KEYS.LOGIN_INSTANCE);
        Preferences.removeData(AppConstants.PREF_KEYS.SELLER_INSTANCE);
        startNextActivity(LoginActivity.class);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {
        isDrawerOpen = true;
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        isDrawerOpen = false;
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }
}
