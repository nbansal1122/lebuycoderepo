package com.lebuy.fragments;

import android.view.View;

import com.lebuy.HomeActivity;
import com.lebuy.R;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Admin on 25-Apr-16.
 */
public class SellerFragment extends BaseFragment {
    HomeActivity activity;
    public SellerFragment(HomeActivity homeActivity) {
        activity= homeActivity;
    }

    @Override
    public void initViews() {
        setOnClickListener(R.id.lay_order_detail);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lay_order_detail:
                OrderDetailFragment orderDetailFragment=new OrderDetailFragment();
                activity.addFragment(orderDetailFragment,true);
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_seller;
    }
}
