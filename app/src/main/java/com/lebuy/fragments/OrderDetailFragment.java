package com.lebuy.fragments;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;

import com.lebuy.R;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Admin on 25-Apr-16.
 */
public class OrderDetailFragment extends BaseFragment {
    @Override
    public void initViews() {
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_order_detail;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
