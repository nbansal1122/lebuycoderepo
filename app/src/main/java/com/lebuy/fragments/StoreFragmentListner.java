package com.lebuy.fragments;

import android.support.v4.app.Fragment;

import com.lebuy.models.StoreData;

/**
 * Created by Admin on 05-May-16.
 */
public interface StoreFragmentListner {
    void save(StoreData storeData);

    void saveAndNext(Fragment fragment);
}
