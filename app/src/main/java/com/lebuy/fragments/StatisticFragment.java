package com.lebuy.fragments;

import android.widget.TextView;

import com.lebuy.R;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Admin on 25-Apr-16.
 */
public class StatisticFragment extends BaseFragment {

    private final TextView tvToolText;

    public StatisticFragment(TextView tvToolText) {
        this.tvToolText=tvToolText;
    }

    @Override
    public void onStart() {
        super.onStart();
        tvToolText.setText(getActivity().getResources().getString(R.string.tv_statistic));
    }

    @Override
    public void initViews() {

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_statistics;
    }
}
