package com.lebuy.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.lebuy.R;
import com.lebuy.constants.Contants;
import com.lebuy.models.GetStoreTypeResponce;
import com.lebuy.models.StoreData;
import com.lebuy.models.StoreType;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class StoreRegistrationFragment1 extends BaseFragment implements MediaFragment.MediaManager, CustomListAdapterInterface {

    private TextInputLayout tilName;
    private Spinner spinnerCatagory;
    private ImageView iv_banner;
    StoreData storeData;
    MediaFragment mediaFragment;
    StoreFragmentListner listner;

    public static StoreRegistrationFragment1 getInstance(StoreData storeData, MediaFragment mediaFragment, StoreFragmentListner listner) {
        StoreRegistrationFragment1 registrationFragment = new StoreRegistrationFragment1();
        registrationFragment.storeData = storeData;
        registrationFragment.mediaFragment = mediaFragment;
        registrationFragment.listner = listner;
        return registrationFragment;
    }

    List<StoreType> storeTypes = new ArrayList<>();


    @Override
    public void initViews() {
        tilName = (TextInputLayout) findView(R.id.til_store_name);
        spinnerCatagory = (Spinner) findView(R.id.spinner_category);
        iv_banner = (ImageView) findView(R.id.iv_banner);
        setOnClickListener(R.id.iv_edit_banner, R.id.btn_next);
        updateOld();
        getCategories();
    }

    private void getCategories() {
        HttpParamObject object = new HttpParamObject();
        object.setUrl(Contants.GET_STORE_TYPE_URL);
        object.setClassType(GetStoreTypeResponce.class);
        executeTask(AppConstants.TASK_CODE_GET_STORE_TYPE, object);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        if (taskCode == AppConstants.TASK_CODE_GET_STORE_TYPE && response != null) {
            GetStoreTypeResponce getStoreTypeResponce = (GetStoreTypeResponce) response;
            storeTypes = getStoreTypeResponce.getStoreTypes();
            String[] strings = new String[storeTypes.size()];
            int selectedPossition = 0;
            for (int x = 0; x < storeTypes.size(); x++) {
                final StoreType storeType = storeTypes.get(x);
                strings[x] = storeType.getStoreType();
                if (storeData.getStoreTypeId().equals(String.valueOf(storeType.getId()))) {
                    selectedPossition = x;
                }
            }
            ArrayAdapter<String> adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, strings);
            spinnerCatagory.setAdapter(adapter);
            spinnerCatagory.setSelection(selectedPossition);
        }
    }

    @Override
    public void showProgressBar() {
    }


    private void updateOld() {
        tilName.getEditText().setText(storeData.getStoreName());
        if (storeData.getStoreBitmap() != null) {
            iv_banner.setImageBitmap(storeData.getStoreBitmap());
        } else if (storeData.getPicUrl() != null) {
            if (!"".equals(storeData.getPicUrl()))
                Picasso.with(getActivity()).load(storeData.getPicUrl().trim()).placeholder(R.drawable.iv_banner).into(iv_banner);
        }
        storeData.setIsPic("0");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_edit_banner:
                getPicture();
                break;

            case R.id.btn_next:
                saveData();
                break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_store_registration_1;
    }

    private void getPicture() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, new String[]{"Camera", "Gallery"});
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    mediaFragment.getImageFromCamera(StoreRegistrationFragment1.this);
                    dialog.cancel();
                } else if (which == 1) {
                    mediaFragment.getImageFromGallery(StoreRegistrationFragment1.this);
                    dialog.cancel();
                }
            }
        });
        dialog = builder.create();
        dialog.setTitle("Choose a picture");
        dialog.show();
    }

    public void saveData() {
        String name = tilName.getEditText().getText().toString();
        if (TextUtils.isEmpty(name)) {
            tilName.setError(getString(R.string.error_valid_store_name));
            return;
        }
        if (storeTypes.size()==0) {
            showToast("Error to get store type..!");
            return;
        }
        storeData.setStoreName(name);
        storeData.setStoreTypeId(String.valueOf(storeTypes.get(spinnerCatagory.getSelectedItemPosition()).getId()));
        StoreRegistrationFragment2 storeRegistrationFragment2 = StoreRegistrationFragment2.getInstance(storeData, listner);
        listner.saveAndNext(storeRegistrationFragment2);
    }

    @Override
    public void onReturnBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            storeData.setStoreBitmap(bitmap);
            storeData.setIsPic("1");
            iv_banner.setImageBitmap(bitmap);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.tv_spinner);
        textView.setText(storeTypes.get(position).getStoreType());
        return convertView;
    }


}
