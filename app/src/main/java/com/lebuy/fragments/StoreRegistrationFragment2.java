package com.lebuy.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lebuy.R;
import com.lebuy.models.StoreData;

import simplifii.framework.fragments.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class StoreRegistrationFragment2 extends BaseFragment implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    private static final int PLACE_PICKER_REQUEST = 20;
    private GoogleMap mMap;
    private MapView mapView;
    private StoreData storeData;
    private Marker marker;
    private EditText et_store_address;
    private TextView tv_counter_area;
    private TextView tv_counter_invitation;
    private LatLng latLng = new LatLng(28, 77);
    StoreFragmentListner listner;

public static StoreRegistrationFragment2 getInstance( StoreData storeData, StoreFragmentListner listner){
    StoreRegistrationFragment2 storeRegistrationFragment2 =new StoreRegistrationFragment2();
    storeRegistrationFragment2.storeData = storeData;
    storeRegistrationFragment2.listner=listner;
    return storeRegistrationFragment2;
}

    @Override
    public void initViews() {
        et_store_address = (EditText) findView(R.id.et_address);
        tv_counter_area = (TextView) findView(R.id.tv_counter_area);
        tv_counter_invitation = (TextView) findView(R.id.tv_counter_invitation);

        mapView = (MapView) findView(R.id.mapView);
        mapView.getMapAsync(this);
        setOnClickListener(R.id.lay_get_location, R.id.iv_add_area, R.id.iv_add_invitation, R.id.iv_subtract_area, R.id.iv_subtract_invitation,R.id.btn_next);
        replaceOldData();
    }

    private void replaceOldData() {
        if (!"".equals(storeData.getStoreAddress())) {
            et_store_address.setVisibility(View.VISIBLE);
            et_store_address.setText(storeData.getStoreAddress());
        }
        if (!(storeData.getLongitude()!=0.0) && (storeData.getLatitude()!=0.0)) {
            LatLng latLng = new LatLng(storeData.getLatitude(), storeData.getLongitude());
            this.latLng = latLng;
        }
        if((storeData.getDeliveryLimit()!=0)&&(storeData.getAcceptLimit()!=0)) {
            tv_counter_area.setText(String.valueOf(storeData.getDeliveryLimit()));
            tv_counter_invitation.setText(String.valueOf(storeData.getAcceptLimit()));
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_store_registration_2;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_get_location:
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.iv_add_area:
                tv_counter_area.setText(String.valueOf(Integer.parseInt(tv_counter_area.getText().toString()) + 1));
                break;
            case R.id.iv_subtract_area:
                int value = Integer.parseInt(tv_counter_area.getText().toString());
                if (value > 0) {
                    tv_counter_area.setText(String.valueOf(value - 1));
                }
                break;
            case R.id.iv_add_invitation:
                tv_counter_invitation.setText(String.valueOf(Integer.parseInt(tv_counter_invitation.getText().toString()) + 1));
                break;
            case R.id.iv_subtract_invitation:
                int val = Integer.parseInt(tv_counter_invitation.getText().toString());
                if (val > 0) {
                    tv_counter_invitation.setText(String.valueOf(val - 1));
                }
                break;
            case R.id.btn_next:
                saveData();
                break;
        }
    }

    public void saveData() {
        if(et_store_address.getText().toString().equals("")){
            Toast.makeText(getActivity(), "Plz select your location...!", Toast.LENGTH_SHORT).show();
            return;
        }
        storeData.setStoreAddress(et_store_address.getText().toString());
        try {
            storeData.setAcceptLimit(Integer.parseInt(tv_counter_invitation.getText().toString()));
            storeData.setDeliveryLimit(Integer.parseInt(tv_counter_area.getText().toString()));
        }catch (Exception e){}
        StoreRegistrationFragment3 storeRegistrationFragment3 = StoreRegistrationFragment3.getInstance(storeData, listner);
        listner.saveAndNext(storeRegistrationFragment3);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
//      mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        MarkerOptions markerOptions = new MarkerOptions().position(latLng);
        marker = mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.pin_location);
//        marker.setIcon(icon);
    }

    void setPossition(LatLng latLng) {
        marker.setPosition(latLng);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == activity.RESULT_OK) {
                Place place = PlacePicker.getPlace(data, getActivity());
                et_store_address.setText(place.getName());
                et_store_address.setVisibility(View.VISIBLE);
                LatLng latLng=place.getLatLng();
                storeData.setLatitude(latLng.latitude);
                storeData.setLongitude(latLng.longitude);
                setPossition(latLng);
            }
//            et_store_address.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mapView.onCreate(savedInstanceState);
    }


    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
