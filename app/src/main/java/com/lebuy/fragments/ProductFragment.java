package com.lebuy.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lebuy.R;
import com.lebuy.constants.Contants;
import com.lebuy.models.Product;
import com.lebuy.models.ProductsBySubCategory;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Admin on 19-May-16.
 */
public class ProductFragment extends BaseFragment implements CustomListAdapterInterface {
    List<Product> products = new ArrayList<>();
    ProductFragmentListener listener;
    HashSet<Product> productHashSet;
    private CustomListAdapter listAdapter;
    boolean isLoaded = false;
    long subCategoryId;
    int TASK_CODE;
    String sellerId;
//    HashSet<Product> productSelectedHashSet;

    public static ProductFragment getInstance(long categoryId, HashSet<Product> productHashSet, ProductFragmentListener listener, int TASK_CODE, String sellerId) {
        ProductFragment productFragment = new ProductFragment();
        productFragment.listener = listener;
        productFragment.productHashSet = productHashSet;
        productFragment.subCategoryId = categoryId;
        productFragment.TASK_CODE = TASK_CODE;
        productFragment.sellerId = sellerId;
//        productFragment.productSelectedHashSet = productSelectedHashSet;
        return productFragment;
    }

    ListView listView;

    @Override
    public void initViews() {
        if (isLoaded == false)
            getdata(subCategoryId);
        listView = (ListView) findView(R.id.list_products);
        listView.setEmptyView(findView(R.id.tv_empty));
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_product, products, this);
        listView.setAdapter(listAdapter);
    }

    @Override
    public void showProgressBar() {
        super.showProgressBar();
    }

    private void getdata(long categoryId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("subcategoryId", String.valueOf(categoryId));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpParamObject op = new HttpParamObject();
        op.setUrl(Contants.GET_PRODUCTS_BY_SUBCATEGORY_ID_URL);
        op.setClassType(ProductsBySubCategory.class);
        op.setJson(jsonObject.toString());
        op.setPostMethod();
        executeTask(TASK_CODE, op);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null && taskCode == TASK_CODE) {
            isLoaded = true;
            ProductsBySubCategory productsBySubCategory = (ProductsBySubCategory) response;
            List<Product> products = productsBySubCategory.getProducts();
            if (null != products) {
                this.products.clear();
                this.products.addAll(products);
                serExtraData(products);
                listAdapter.notifyDataSetChanged();
            }
        }
    }

    private void serExtraData(List<Product> products) {
        for (Product product : products) {
            product.setSellerId(sellerId);
            product.setSubcategoryId(subCategoryId);
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_product;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent, final int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getActivity()).inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final Product product = products.get(position);

        if (productHashSet.contains(product)) {
            holder.cbSelectProduct.setChecked(true);
        } else {
            holder.cbSelectProduct.setChecked(false);
        }
        holder.layProductLayer.setVisibility(View.VISIBLE);
        holder.tvName.setText(product.getProductName());
        holder.tvMRP.setText("\u20B9 " + product.getMRP());
        holder.tvNetPrize.setText("\u20B9 " + product.getNetPrize());
        holder.tvQuantity.setText(String.valueOf(product.getPoductQuantity()));
        holder.tvPriceQuantuty.setText("Unit Qty: "+product.getPriceQuantity());
        Picasso.with(getActivity()).load(product.getPicUrl().trim()).into(holder.ivProduct);
        holder.layProductLayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onProductClicked(position);
            }
        });
        return convertView;
    }

    private void onProductClicked(int position) {
        final Product product = products.get(position);
        if (productHashSet.contains(product)) {
            productHashSet.remove(product);
        } else {
            productHashSet.add(product);
        }
        listener.onSelectProduct();
        listAdapter.notifyDataSetChanged();
    }

    class Holder {
        TextView tvName, tvDescription, tvMRP, tvNetPrize,tvPriceQuantuty,tvQuantity;
        ImageView ivProduct,ivPlus,ivMinus;
        CheckBox cbSelectProduct;
        RelativeLayout layProductLayer;

        public Holder(View view) {
            tvName = (TextView) view.findViewById(R.id.tv_product_name);
            tvDescription = (TextView) view.findViewById(R.id.tv_product_description);
            tvMRP = (TextView) view.findViewById(R.id.tv_product_MRP);
            tvPriceQuantuty= (TextView) view.findViewById(R.id.tv_product_price_quantity);
            tvNetPrize = (TextView) view.findViewById(R.id.tv_product_netprize);
            tvQuantity = (TextView) view.findViewById(R.id.tv_product_quantity);
            ivProduct = (ImageView) view.findViewById(R.id.iv_product_row);
            cbSelectProduct = (CheckBox) view.findViewById(R.id.cb_select_product);
            ivMinus = (ImageView) view.findViewById(R.id.iv_minus);
            ivPlus = (ImageView) view.findViewById(R.id.iv_plus);
            layProductLayer= (RelativeLayout) view.findViewById(R.id.lay_uper_product);
        }
    }

    public interface ProductFragmentListener {
        void onSelectProduct();
    }
}
