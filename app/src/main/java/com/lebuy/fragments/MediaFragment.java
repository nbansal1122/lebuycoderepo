package com.lebuy.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.io.IOException;

import simplifii.framework.utility.Util;


/**
 * Created by Admin on 13-Apr-16.
 */
public class MediaFragment extends Fragment {
    public final int REQUEST_CODE_GALLARY = 50;
    public final int REQUEST_CODE_CAMERA = 51;
    MediaManager mediaManager;


    public void getImageFromCamera(MediaManager mediaManager) {
        this.mediaManager = mediaManager;
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, REQUEST_CODE_CAMERA);
    }
    public void getImageFromGallery(MediaManager mediaManager) {
        this.mediaManager = mediaManager;
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_GALLARY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("msg", "onActivity result is called... requestCode=" + requestCode);
        if(resultCode!=getActivity().RESULT_OK)
            return;
        switch (requestCode) {
            case REQUEST_CODE_CAMERA:
                Bitmap bmp= (Bitmap) data.getExtras().get("data");
                mediaManager.onReturnBitmap(scaleDown(bmp, 2048, true));
                break;
            case REQUEST_CODE_GALLARY:
                Bitmap bitmap=getBitmapFromUri(getContext(),data.getData());
                mediaManager.onReturnBitmap(scaleDown(bitmap, 2048, true));
                break;
        }
    }
    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }
    public interface MediaManager{
        void onReturnBitmap(Bitmap bitmap);
    }
    public Bitmap getBitmapFromUri(Context ctx, Uri imageUri) {
        try {
            return MediaStore.Images.Media.getBitmap(ctx.getContentResolver(), imageUri);
        } catch (IOException e) {
            e.printStackTrace();

        }
        return null;
    }

}
