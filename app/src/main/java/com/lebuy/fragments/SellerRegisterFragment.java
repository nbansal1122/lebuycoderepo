package com.lebuy.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.lebuy.R;
import com.lebuy.models.SellerData;

import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.Util;

public class SellerRegisterFragment extends BaseFragment implements MediaFragment.MediaManager, View.OnFocusChangeListener {
    private static final int REQ_CODE_OTP = 10;
    private static final int TASK_CODE_REGISTER = 11;
    private TextInputLayout name_TIL, mobile_TIL, email_TIL, pass_TIL, confirm_pass_TIL;
    SellerData sellerData;
    private MediaFragment mediaFragment;
    ImageView iv_camera, iv_register;
    RegisterFragmentListner listener;
    private Bitmap bitmap;
    boolean isUpdate;
    public static SellerRegisterFragment getInstance(SellerData sellerData, MediaFragment mediaFragment, RegisterFragmentListner listener,boolean isUpdate) {
        SellerRegisterFragment sellerRegisterFragment = new SellerRegisterFragment();
        sellerRegisterFragment.sellerData = sellerData;
        sellerRegisterFragment.mediaFragment = mediaFragment;
        sellerRegisterFragment.listener = listener;
        sellerRegisterFragment.isUpdate=isUpdate;
        return sellerRegisterFragment;
    }

    @Override
    public void initViews() {
        name_TIL = (TextInputLayout) findView(R.id.name_TIL);
        mobile_TIL = (TextInputLayout) findView(R.id.number_TIL);
        email_TIL = (TextInputLayout) findView(R.id.email_TIL);
        pass_TIL = (TextInputLayout) findView(R.id.password_TIL);
        confirm_pass_TIL = (TextInputLayout) findView(R.id.confirm_password_TIL);
        iv_camera = (ImageView) findView(R.id.iv_camera);
        iv_register = (ImageView) findView(R.id.iv_register);
        if(isUpdate){
            email_TIL.setEnabled(false);
            email_TIL.getEditText().setEnabled(false);
        }
        setOnFocusChangeListener(name_TIL,mobile_TIL,pass_TIL,email_TIL);
        setOnClickListener(R.id.btn_next, R.id.iv_register);
        updateOld(sellerData);
    }

    private void setOnFocusChangeListener(TextInputLayout... layouts) {
        for (TextInputLayout inputLayout : layouts) {
            inputLayout.getEditText().setOnFocusChangeListener(this);
        }
    }

    private void updateOld(SellerData sellerData) {
        name_TIL.getEditText().setText(sellerData.getName());
        mobile_TIL.getEditText().setText(sellerData.getMobile());
        email_TIL.getEditText().setText(sellerData.getEmail());
        pass_TIL.getEditText().setText(sellerData.getPassword());
        confirm_pass_TIL.getEditText().setText(sellerData.getPassword());
        if (sellerData.getImageBitmap() != null) {
            iv_register.setImageBitmap(sellerData.getImageBitmap());
            iv_camera.setVisibility(View.GONE);
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_seller_registration;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_register:
                getPicture();
                break;
            case R.id.btn_next:
                registerSeller();
                break;
        }
    }

    private void getPicture() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, new String[]{"Camera", "Gallery"});
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    mediaFragment.getImageFromCamera(SellerRegisterFragment.this);
                    dialog.cancel();
                } else if (which == 1) {
                    mediaFragment.getImageFromGallery(SellerRegisterFragment.this);
                    dialog.cancel();
                }
            }
        });
        dialog = builder.create();
        dialog.setTitle("Choose a picture");
        dialog.show();
    }

    public void registerSeller() {
        String name = name_TIL.getEditText().getText().toString();
        String mobile = mobile_TIL.getEditText().getText().toString();
        String email = email_TIL.getEditText().getText().toString();
        String password = pass_TIL.getEditText().getText().toString();
        String confirm_password = confirm_pass_TIL.getEditText().getText().toString();

        if (TextUtils.isEmpty(name)) {
            name_TIL.setError(getString(R.string.error_valid_name));
            return;
        }
        if (mobile.length() < 10) {
            mobile_TIL.setError(getString(R.string.error_valid_mobile));
            return;
        }
        if (TextUtils.isEmpty(email)) {
            email_TIL.setError(getString(R.string.error_valid_email));
            return;
        }
        if (!Util.isValidEmail(email)) {
            email_TIL.setError(getString(R.string.error_valid_email));
            return;
        }
        if (TextUtils.isEmpty(password)) {
            pass_TIL.setError(getString(R.string.error_valid_password));
            return;
        }
        if (!password.equals(confirm_password)) {
            confirm_pass_TIL.setError(getString(R.string.error_password_not_match));
            return;
        }
        sellerData.setName(name);
        sellerData.setEmail(email);
        sellerData.setMobile(mobile);
        sellerData.setImageBitmap(bitmap);
        sellerData.setPassword(password);
        listener.onSellerDataRecieve();
    }
    @Override
    public void onReturnBitmap(Bitmap bitmap) {
        sellerData.setImageBitmap(bitmap);
        iv_register.setImageBitmap(bitmap);
        this.bitmap = bitmap;
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.et_name:
                if(hasFocus){
                    name_TIL.setError(null);
                }
                break;
            case R.id.et_email:
                if(hasFocus){
                    email_TIL.setError(null);
                    email_TIL.setErrorEnabled(false);
                }
                break;
            case R.id.et_pass:
                if(hasFocus){
                    pass_TIL.setError(null);
                    pass_TIL.setErrorEnabled(false);
                }
                break;
            case R.id.et_number:
                if(hasFocus){
                    mobile_TIL.setError(null);
                    mobile_TIL.setErrorEnabled(false);
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public interface RegisterFragmentListner {
        void onSellerDataRecieve();
    }
}
