
package com.lebuy.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lebuy.HomeActivity;
import com.lebuy.MyListingActivity;
import com.lebuy.R;
import com.lebuy.SellerRegistrationActivity;
import com.lebuy.StoreRegistrationActivity;
import com.lebuy.models.Customer;
import com.lebuy.models.LoginResponse;
import com.lebuy.models.SellerData;
import com.lebuy.models.StoreData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by Admin on 25-Apr-16.
 */
public class DrawerFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    ListView listView;
    List<String> values = new ArrayList<>();
    HomeActivity homeActivity;
    DrawerLayout drawer;
    TextView tv_title, tv_subtitle;
    private ImageView ivDrawer;
    private AlertDialog alertDialog;

    public static DrawerFragment getInstance(List<String> values, HomeActivity homeActivity, DrawerLayout drawer) {
        DrawerFragment drawerFragment = new DrawerFragment();
        drawerFragment.values = values;
        drawerFragment.homeActivity = homeActivity;
        drawerFragment.drawer = drawer;
        return drawerFragment;
    }

    @Override
    public void initViews() {
        setDrawerData();
        listView = (ListView) findView(R.id.list_drawer);
        listView.setAdapter(new CustomListAdapter(getActivity(), R.layout.row_drawer_list, values, this));
        listView.setOnItemClickListener(this);
        setOnClickListener(R.id.iv_drawer_edit);
    }

    private void setDrawerData() {
        tv_title = (TextView) findView(R.id.tv_drawer_title);
        tv_subtitle = (TextView) findView(R.id.tv_drawer_sub_title);
        ivDrawer = (ImageView) findView(R.id.iv_drawer);
        String userInstance = Preferences.getData(AppConstants.PREF_KEYS.LOGIN_INSTANCE, "");
        if (!"".equals(userInstance)) {
            Gson gson = new Gson();
            LoginResponse loginResponse = gson.fromJson(userInstance, LoginResponse.class);
            SellerData sellerData = loginResponse.getSeller().get(0);
            tv_title.setText(sellerData.getName());
            tv_subtitle.setText(sellerData.getEmail());
            if (!"".equals(sellerData.getImageUrl()))
                Picasso.with(getActivity()).load(sellerData.getImageUrl()).into(ivDrawer);
            else
                Picasso.with(getActivity()).load("https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAAR9AAAAJDJhNjE2N2VlLTU0YmYtNDhkZi04MTVkLWIzNzlkNGY5ZTEwOA.jpg").into(ivDrawer);
        } else {
            tv_title.setText(Preferences.getData(AppConstants.PREF_KEYS.SELLER_NAME, "Default Name"));
            tv_subtitle.setText(Preferences.getData(AppConstants.PREF_KEYS.SELLER_EMAIL, "Default Email"));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_drawer_edit:
                update();
                break;
        }
    }

    private void update() {
        String storeInstance = Preferences.getData(AppConstants.PREF_KEYS.LOGIN_INSTANCE, "");
        if ("".equals(storeInstance))
            return;
        Gson gson = new Gson();
        final StoreData storeData = gson.fromJson(storeInstance, StoreData.class);
        final Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, storeData);

        alertDialog = Util.createListAlertDialog(getActivity(), new String[]{"Update Profile", "Update Store"}, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0) {
                    updateSeller(bundle);
                    alertDialog.dismiss();
                } else {
                    updateStore(bundle);
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();
    }

    private void updateStore(Bundle bundle) {
        Intent intent = new Intent(getActivity(), StoreRegistrationActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, AppConstants.REQUEST_CODES.UPDATE);
    }

    private void updateSeller(Bundle bundle) {
        Intent intent = new Intent(getActivity(), SellerRegistrationActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, AppConstants.REQUEST_CODES.UPDATE);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_drawer;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            Holder holder = new Holder(convertView);
            convertView.setTag(holder);
        }
        Holder holder = (Holder) convertView.getTag();
        holder.tvDrawerList.setText(values.get(position));
        holder.ivDrawerList.setImageResource(R.drawable.drawer_icon);
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                OrderFragment orderFragment = OrderFragment.getInstance(tv_title);
                homeActivity.addFragment(orderFragment, false);
                break;
            case 1:
                startActivity(new Intent(getActivity(), MyListingActivity.class));
                break;
            case 2:
                List<Customer> customers = getCustomers();
                CustomersFragment customersFragment = CustomersFragment.getInstance(customers, getActivity(), homeActivity.tvToolText);
                homeActivity.addFragment(customersFragment, true);
                break;
            case 4:
                StatisticFragment statisticFragment = new StatisticFragment(homeActivity.tvToolText);
                homeActivity.addFragment(statisticFragment, true);
                break;
            case 5:
                homeActivity.logout();
                break;
        }
        drawer.closeDrawer(Gravity.LEFT);
    }

    public List<Customer> getCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();
        for (int x = 1; x <= 10; x++) {
            Customer customer = new Customer();
            customer.setName("Nitin-" + x);
            customer.setMobile("+91 789133287" + x);
            customer.setDetail("The android developer, from delhi. dciuvvh9dvvdn eiwuffe9ewfeu98 sduhiuu wieudhwu wiudhwed isdd weddw ediwudwuud iuwdhwed " + x);
            customer.setRating((float) (x / 2));
            customer.setLikes(100 + x);
            customer.setImageUrl("https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAAR9AAAAJDJhNjE2N2VlLTU0YmYtNDhkZi04MTVkLWIzNzlkNGY5ZTEwOA.jpg");
            customers.add(customer);
        }
        return customers;
    }

    class Holder {
        TextView tvDrawerList;
        ImageView ivDrawerList;

        public Holder(View view) {
            tvDrawerList = (TextView) view.findViewById(R.id.tv_drawer);
            ivDrawerList = (ImageView) view.findViewById(R.id.iv_drawer_list);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setDrawerData();
    }
}
