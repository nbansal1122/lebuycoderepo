package com.lebuy.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.lebuy.CustomerOrderDetailActivity;
import com.lebuy.R;
import com.lebuy.models.OrderDetail;
import com.squareup.picasso.Picasso;

import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * A simple {@link Fragment} subclass.
 */
public class ApprovedFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    List<OrderDetail> orderDetails;
    ListView listView;

    public static ApprovedFragment getInstance(List<OrderDetail> orderDetails){
        ApprovedFragment f = new ApprovedFragment();
        f.orderDetails = orderDetails;
        return f;
    }

    @Override
    public void initViews() {
        listView = (ListView) findView(R.id.list_approved);
        listView.setAdapter(new CustomListAdapter<>(getActivity(), R.layout.row_orders, orderDetails, this));
        listView.setOnItemClickListener(this);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_approved;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getActivity()).inflate(R.layout.row_orders, parent, false);
            Holder holder = new Holder(convertView);
            convertView.setTag(holder);
        }
        OrderDetail orderDetail = orderDetails.get(position);
        Holder holder = (Holder) convertView.getTag();
        holder.tvHeader.setText(orderDetail.getCustomerName());
        holder.tvOrderQuantity.setText(orderDetail.getMobileNo());
        holder.tvDate.setText(orderDetail.getOrderDate());
        holder.tvOrderDeliveredDate.setText(orderDetail.getDeliveredDate());
        holder.tvSKUCode.setText(orderDetail.getOrderId());
        holder.tvRate.setText(orderDetail.getTotalAmmount());
        Picasso.with(getActivity()).load(orderDetail.getImageURL()).into(holder.ivOrder);
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle bundle=new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,orderDetails.get(position));
        Intent intent=new Intent(getActivity(), CustomerOrderDetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    class Holder {
        TextView tvHeader, tvOrderQuantity, tvDate,  tvOrderDeliveredDate, tvSKUCode;
        TextView tvRate;
        ImageView ivOrder;

        public Holder(View view) {
            tvHeader = (TextView) view.findViewById(R.id.tv_header);
            tvDate = (TextView) view.findViewById(R.id.tv_order_date);
            tvOrderQuantity = (TextView) view.findViewById(R.id.tv_order_quantity);
            tvOrderDeliveredDate = (TextView) view.findViewById(R.id.tv_order_delivered_date);
            tvSKUCode = (TextView) view.findViewById(R.id.tv_sku_code);
            tvRate = (TextView) view.findViewById(R.id.btn_ammount);
            ivOrder = (ImageView) view.findViewById(R.id.iv_order);
        }
    }
}
