package com.lebuy.fragments;


import android.app.TimePickerDialog;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.lebuy.R;
import com.lebuy.models.StoreData;
import simplifii.framework.fragments.BaseFragment;

public class StoreRegistrationFragment3 extends BaseFragment {
    TextView tv_shop_open_time, tv_shop_close_time;
    EditText et_minimam_order, et_loyality_credits;
    CheckBox cb_COD, cb_AllowCard, cb_AllowSudexo, cbAgree;
    StoreData storeData;
    StoreFragmentListner listner;

    public static StoreRegistrationFragment3 getInstance(StoreData storeData, StoreFragmentListner listner) {
        StoreRegistrationFragment3 storeRegistrationFragment3 = new StoreRegistrationFragment3();
        storeRegistrationFragment3.storeData = storeData;
        storeRegistrationFragment3.listner = listner;
        return storeRegistrationFragment3;
    }

    @Override
    public void initViews() {
        tv_shop_open_time = (TextView) findView(R.id.iv_shop_open_time);
        tv_shop_close_time = (TextView) findView(R.id.iv_shop_close_time);
        et_loyality_credits = (EditText) findView(R.id.et_loyality);
        cb_COD = (CheckBox) findView(R.id.rb_COD);
        cb_AllowSudexo = (CheckBox) findView(R.id.rb_Sodexo);
        cb_AllowCard = (CheckBox) findView(R.id.rb_Card_On_delivery);
        cbAgree = (CheckBox) findView(R.id.cb_agree);
        et_minimam_order = (EditText) findView(R.id.et_minimum_order);
        setOnClickListener(R.id.iv_shop_open_time, R.id.iv_shop_close_time, R.id.btn_next);
        updateOld();
    }

    public void gettime(final TextView textView) {
        TimePickerDialog tpd = new TimePickerDialog(activity,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String hours = String.valueOf(hourOfDay);
                        String minutes = String.valueOf(minute);

                        if (hourOfDay < 10) {
                            hours = "0" + hourOfDay;
                        }
                        if (minute < 10) {
                            minutes = "0" + minute;
                        }
                        String formatedTime = formatTime(hours + ":" + minutes);
                        textView.setText(formatedTime);

                        if (textView.getId() == R.id.iv_shop_open_time) {
                            storeData.setShopOpenTime(textView.getText().toString());
                        } else {
                            storeData.setShopCloseTime(textView.getText().toString());
                        }
                    }
                }, 8, 0, true);
        tpd.show();
    }

    private String formatTime(String time) {
        int hour = Integer.parseInt(time.substring(0, time.indexOf(':')));
        int minut = Integer.parseInt(time.substring(time.indexOf(':') + 1));
        if (hour == 0) {
            return "12" + ":" + formatedMinut(minut) + "AM";
        }
        if (hour < 12) {
            return time + " AM";
        }
        if (hour == 12) {
            return time + " PM";
        } else {

            return formatedMinut(hour - 12) + ":" + formatedMinut(minut) + " PM";
        }
    }

    private String formatedMinut(int minut) {
        if (minut < 10) {
            return "0" + minut;
        } else {
            return String.valueOf(minut);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.iv_shop_open_time:
                gettime(tv_shop_open_time);
                break;
            case R.id.iv_shop_close_time:
                gettime(tv_shop_close_time);
                break;
            case R.id.btn_next:
                saveData();
                break;
        }
    }


    public void saveData() {
        if(!cbAgree.isChecked()){
            showToast("Please agree the Conditions..!");
            return;
        }
        if (et_minimam_order.getText().toString().equals("")) {
            et_minimam_order.setError("Plz enter minimum order limit");
            return;
        }
        if (tv_shop_open_time.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "plz select shop open time", Toast.LENGTH_SHORT).show();
            return;
        }
        if (tv_shop_close_time.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "plz select shop close time", Toast.LENGTH_SHORT).show();
            return;
        }
        if (et_loyality_credits.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "select Loyality Credits", Toast.LENGTH_SHORT).show();
            return;
        }

        storeData.setShopOpenTime(tv_shop_open_time.getText().toString());
        storeData.setShopCloseTime(tv_shop_close_time.getText().toString());
        storeData.setMinimumOrder(et_minimam_order.getText().toString());
        storeData.setLoyality(et_loyality_credits.getText().toString());
        storeData.setTryAtHome("0");
//-------------------------------------------------
        if (cb_AllowCard.isChecked())
            storeData.setAllowCard("1");
        else
            storeData.setAllowCard("0");
//-------------------------------------------------
        if (cb_COD.isChecked())
            storeData.setAllowCOD("1");
        else
            storeData.setAllowCOD("0");
//-------------------------------------------------
        if (cb_AllowSudexo.isChecked())
            storeData.setAllowSudexo("1");
        else
            storeData.setAllowSudexo("0");
//-------------------------------------------------
        listner.save(storeData);
    }

    private void updateOld() {
        et_minimam_order.setText(storeData.getMinimumOrder());
        tv_shop_open_time.setText(storeData.getShopOpenTime());
        tv_shop_close_time.setText(storeData.getShopCloseTime());
        et_loyality_credits.setText(storeData.getLoyality());
//        if (storeData.getTryAtHome().equals("1"))
//            aSwitchTryAtHome.setChecked(true);
        if (storeData.getAllowCOD().equals("1"))
            cb_COD.setChecked(true);
        if (storeData.getAllowCard().equals("1"))
            cb_AllowCard.setChecked(true);
        if (storeData.getAllowSudexo().equals("1"))
            cb_AllowSudexo.setChecked(true);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_store_registration_3;
    }



}
