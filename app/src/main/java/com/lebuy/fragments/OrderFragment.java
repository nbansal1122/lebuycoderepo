package com.lebuy.fragments;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.lebuy.R;
import com.lebuy.models.OrderDetail;
import com.lebuy.models.OrderItem;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Admin on 26-Apr-16.
 */
public class OrderFragment extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface<String> {
    private ViewPager pager;
    private TabLayout tabLayout;
    private List<String> tabs = new ArrayList<>();
    private CustomPagerAdapter<String> adapter;
    TextView tvTitle;

    public static OrderFragment getInstance(TextView textView){
        OrderFragment orderFragment=new OrderFragment();
        orderFragment.tvTitle=textView;
        return orderFragment;
    }

    @Override
    public void initViews() {
        initTabs();
        pager = (ViewPager) findView(R.id.viewpager_order);
        tabLayout = (TabLayout) findView(R.id.tab_order);
        adapter = new CustomPagerAdapter<>(getChildFragmentManager(), tabs, this);
        pager.setAdapter(adapter);
        tabLayout.setupWithViewPager(pager);
    }

    @Override
    public void onResume() {
        super.onResume();
        tvTitle.setText(getActivity().getResources().getString(R.string.title_orders));

    }

    private void initTabs() {
        tabs.clear();
        tabs.add(activity.getResources().getString(R.string.tv_approved));
        tabs.add(activity.getResources().getString(R.string.tv_packed));
        tabs.add(activity.getResources().getString(R.string.tv_deliverd));
        tabs.add(activity.getResources().getString(R.string.tv_cancelled));
    }

    List<OrderDetail> initData(){
        List<OrderDetail> orderDetails =new ArrayList<OrderDetail>();
        orderDetails.clear();
        for (int x=1;x<=10;x++){
            OrderDetail orderDetail =new OrderDetail();
            orderDetail.setCustomerName("Rachel garish:"+x);
            orderDetail.setMobileNo("+91-789133287"+String.valueOf(x-1));
            orderDetail.setImageURL("http://ariteta.by/templates/protostar/images/fullitservice/customer-service.jpg");

            orderDetail.setOrderDate("Jan " + x + ", 2016");
            orderDetail.setDeliveredDate("Jan " + x * 2 + ", 2016");
            orderDetail.setOrderStatus("Delivered");

            orderDetail.setOrderId("xx2948" + x);
            orderDetail.setTotalAmmount("Rs. 24,00" + x);
            orderDetail.setSubTotalAmmount("Rs. 20,00"+x);
            orderDetail.setVatAmmount("Rs. 4,00"+x);

            orderDetail.setPickupAddress("Sector 22,New Dehli");
            orderDetail.setDeliveryAddress("Dc office, Sector 16,New Dehli");
            orderDetail.setPickupTime("(11:00 am)");
            orderDetail.setDeliveryTime("(01:00 am)");

            List<OrderItem> items=new ArrayList<OrderItem>();
            for (int i=1;i<=7;i++){
                OrderItem orderItem=new OrderItem();
                orderItem.setItemName("5kg Dal "+i);
                orderItem.setItemRate("Rs. 100"+i);
                items.add(orderItem);
            }
            orderDetail.setOrderItems(items);

            orderDetails.add(orderDetail);
        }
        return orderDetails;
    }

    @Override
    public Fragment getFragmentItem(int position, String listItem) {
        List<OrderDetail> orderDetails =initData();
        return ApprovedFragment.getInstance(orderDetails);
    }

    @Override
    public CharSequence getPageTitle(int position, String listItem) {
        return listItem;
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_order;
    }
}
