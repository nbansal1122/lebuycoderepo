package com.lebuy.fragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.lebuy.R;
import com.lebuy.models.Customer;
import com.squareup.picasso.Picasso;

import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Admin on 25-Apr-16.
 */
public class CustomersFragment extends BaseFragment implements CustomListAdapterInterface {
    private List<Customer> list;
    private Context context;
    TextView tvToolTitle;


    public static CustomersFragment getInstance(List<Customer> list, Context context, TextView tvToolTitle) {
        CustomersFragment f = new CustomersFragment();
        f.list = list;
        f.context = context;
        f.tvToolTitle = tvToolTitle;
        return f;
    }

    @Override
    public void onStart() {
        super.onStart();
        tvToolTitle.setText("Customers");
    }

    @Override
    public void initViews() {
        ListView listView = (ListView) findView(R.id.list_customer);
        CustomListAdapter customListAdapter = new CustomListAdapter(context, R.layout.row_customers, list, this);
        listView.setAdapter(customListAdapter);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_customer;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_customers, parent, false);
            Holder holder = new Holder(convertView);
            convertView.setTag(holder);
        }
        Holder holder = (Holder) convertView.getTag();
        Customer customer = list.get(position);

        holder.tvHeader.setText(customer.getName());
        holder.tvPhone.setText(customer.getMobile());
        holder.tvDetail.setText(customer.getDetail());
        holder.ratingBar.setRating(customer.getRating());
        holder.btnLikes.setText("+" + String.valueOf(customer.getLikes()));
        Picasso.with(getActivity()).load(customer.getImageUrl()).into(holder.imageView);
        return convertView;
    }

    class Holder {
        TextView tvHeader, tvPhone, tvDetail;
        RatingBar ratingBar;
        Button btnLikes;
        ImageView imageView;

        public Holder(View view) {
            tvHeader = (TextView) view.findViewById(R.id.tv_header);
            tvPhone = (TextView) view.findViewById(R.id.tv_phone);
            tvDetail = (TextView) view.findViewById(R.id.tv_customer_detail);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingbar);
            btnLikes = (Button) view.findViewById(R.id.btn_like);
            imageView = (ImageView) view.findViewById(R.id.iv_customer);
        }
    }
}
