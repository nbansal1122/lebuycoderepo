package simplifii.framework.utility;

import java.util.HashMap;
import java.util.LinkedHashMap;

public interface AppConstants {

    String DEF_REGULAR_FONT = "OpenSans-Regular.ttf";
    String APP_LINK = "https://drive.google.com/file/d/0B8wKJnD6sONHeXlUbm5pOTk4dGM/view?usp=sharing";
    int TASK_CODE_ADD_PRODUCTS = 21;
    int TASK_CODE_GET_MY_LISTING = 14;
    int TASK_CODE_GET_STORE_TYPE = 15;
    int TASK_CODE_REGISTER = 16;
    int REQ_CODE_OTP =17 ;
    int TASK_CODE_ADD_PRODUCTS_BY_QUANTITY = 18;


    interface PARAMS {
        String LAT = "latitude";
        String LNG = "longitude";
    }


    interface ERROR_CODES {

        int UNKNOWN_ERROR = 0;
        int NO_INTERNET_ERROR = 1;
        int NETWORK_SLOW_ERROR = 2;
        int URL_INVALID = 3;
        int DEVELOPMENT_ERROR = 4;

    }

    interface PAGE_URL {
    }

    interface PREF_KEYS {

        String KEY_LOGIN = "IsUserLoggedIn";
        String KEY_USERNAME = "username";
        String KEY_PASSWORD = "password";
        String ACCESS_CODE = "access";
        String APP_LINK = "appLink";


        String USER_ID ="user_id";
        String IS_LOGIN = "is_login";
        String LOGIN_INSTANCE = "user_intance";
        String SELLER_NAME = "user_name";
        String SELLER_MOBILE = "Seller_mobile";
        String SELLER_EMAIL = "seller_email";
        String SELLER_INSTANCE = "seller Instance";
        String STORE_ID = "store_id";
    }

    interface BUNDLE_KEYS {
        String KEY_SERIALIZABLE_OBJECT = "KEY_SERIALIZABLE_OBJECT";
        String FRAGMENT_TYPE = "FRAGMENT_TYPE";
        String EXTRA_BUNDLE = "bundle";
        String NUMBER = "number";
        String CATEGORY_ID = "cat_id";
    }


    public interface TASK_CODES {
        int TASK_CODE_STORE_UPDATE = 10;
        int TASK_CODE_LOGIN = 11;
        int TASK_CODE_GET_ALL_CATEGORIES = 12;
        int TASK_CODE_GET_ALL_SUB_CATEGORIES = 13;
        int REQ_CODE_GET_PRODUCT = 14;
        int TASK_CODE_ADD_PRODUCTS = 15;
        int TASK_CODE_GET_MY_LISTING = 16;
        int TASK_CODE_GET_STORE_TYPE = 17;
        int TASK_CODE_REGISTER = 18;
        int REQ_CODE_OTP =19 ;
        int TASK_CODE_ADD_PRODUCTS_BY_QUANTITY = 20;
        int TASK_CODE_STORE_REG = 21;
    }

    public interface REQUEST_CODES {
        int UPDATE = 10;
        int REQ_CODE_GET_PRODUCT = 11;
    }
}
