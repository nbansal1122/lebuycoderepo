package simplifii.framework.utility;

import android.os.AsyncTask;

/**
 * Created by Admin on 24-Jul-16.
 */
public class SplashTask extends AsyncTask<Void,Void,Void> {
    int waitingTime;
    OnCompliteListener onCompliteListener;

    public SplashTask(int waitingTime, OnCompliteListener onCompliteListener) {
        this.waitingTime = waitingTime;
        this.onCompliteListener = onCompliteListener;
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            Thread.sleep(waitingTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        onCompliteListener.onComplete();
    }

    public interface OnCompliteListener{
        void onComplete();
    }
}
